KERNEL_IMG_NAME	:= .vmlinux

START_SCRIPT_CAPI := $(FILESYSTEM)/etc/init.d/E41-capi
START_SCRIPT_PIGLET := $(FILESYSTEM)/etc/init.d/S11-piglet

START_SCRIPTS_ARCH := ""

# Disable separate build directory
LINUX_KERNEL_BUILDDIR := $(LINUX_KERNEL_SOURCE)

ADD_FILE_HELPER := $(wildcard add_file_helper/*)
ADD_FILE_HELPER := $(wildcard scripts/add-file-helper/*)

.lha.options: $(LINUX_KERNEL_BUILDDIR)/System.map
	rm -f $@
	echo -e "-l \c" >> $@
	grep '[TA] _text' $< | sed -e's/[a-fA-F0-9]\(.*\) [TA] .*/0x8\1/g' | tr '\n' ' ' >> $@

	echo -e "-e \c" >> $@
	grep 'T kernel_entry' $< | sed -e's/[a-fA-F0-9]\(.*\) T .*/0x8\1/g' | tr '\n' ' ' >> $@

$(FILESYSTEM)/$(KERNEL_IMG_NAME): $(LINUX_KERNEL_BUILDDIR)/vmlinux
	@echo "  OBJCOPY $(notdir $<) -> $@"
	$(CROSS_COMPILE)objcopy --remove-section .note.gnu.build-id -S -O binary $< $@
	# fuer Binaer-Images die direkt gebootet werden und eine nachfolgendes FS haben
	@echo "  OBJCOPY $(notdir $<) -> $@.bss"
	$(CROSS_COMPILE)objcopy --remove-section .note.gnu.build-id --set-section-flags .bss=alloc,load,contents --set-section-flags .sbss=alloc,load,contents -S -O binary $< $@.bss
	# fuer dissassemble-helper in avmterm:
	echo `nm $< | grep "kallsyms_\(names\|token\|num_syms\|addresses\)"` >> $@
	echo `file -b -e elf $<` >>$@


.PHONY: install_arch_scripts
install: install_arch_scripts
install_arch_scripts:
#	$(foreach script,$(START_SCRIPTS_ARCH),$(call cmd_install,$(script),$(FILESYSTEM)/etc/init.d/))

.PHONY: install_add_file_helper
install: install_add_file_helper 
install_add_file_helper: $(ADD_FILE_HELPER) 
	@echo "  INSTALL kernel scripts"
	mkdir -p $(FILESYSTEM)/Config
	$(foreach script,$(ADD_FILE_HELPER),$(call cmd_install,$(script),$(FILESYSTEM)/Config/))

dtbs = $(wildcard $(LINUX_KERNEL_BUILDDIR)/arch/$(ARCH)/boot/dts/Fritz_Box_*.dtb)
install: install_dtbs install_envs
install_dtbs: $(dtbs)
ifeq ($(dtbs),)
	$(error No device-tree blobs found!  Is CONFIG_OF_AVM_DT set?)
endif
	@echo "  INSTALL generated device tree blobs"
	$(foreach dtb,$(dtbs),$(call cmd_install,$(dtb),$(FILESYSTEM)/avm_dts/))

envs = $(wildcard $(LINUX_KERNEL_BUILDDIR)/arch/$(ARCH)/boot/dts/Fritz_Box_*_env.h)
install_envs: $(envs)
ifneq ($(envs),)
	@echo "  INSTALL generated static environments"
	$(foreach env,$(envs),$(call cmd_install,$(env),$(FILESYSTEM)/avm_dts/$(notdir $(subst _env.h,.env,$(env)))))
endif

# vim: set ft=make noexpandtab ts=8:

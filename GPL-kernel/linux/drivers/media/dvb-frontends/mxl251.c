/*
 * mxl251: Driver for the MXL251 Dual Digital Cable Tuner/Demodulator
 *
 * Copyright (C) 2010 Digital Devices GmbH
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 only, as published by the Free Software Foundation.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA
 * Or, point your browser to http://www.gnu.org/copyleft/gpl.html
 */

/* Dummy Driver for MXL251 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>

#include "dvb_frontend.h"
#include "mxl251.h"

static unsigned int debug;
module_param(debug, int, 0644);
MODULE_PARM_DESC(debug, "enable debug messages");

static void mxl251_release(struct dvb_frontend *fe);

struct mxl251_state {

	struct dvb_frontend 	frontend;
	struct i2c_adapter 	*i2c;

	u8 			adr;
	u32 			tuner_frequency;
	u32 			symbol_rate;
	enum fe_code_rate 	fec_inner;

};

static struct dvb_frontend_ops mxl251_ops = {
	.info = {
		.name = "MxL251",
		.frequency_min  =   44000000,
		.frequency_max  = 1002000000,
		 /* For DVB-C */
		.symbol_rate_min = 870000,
		.symbol_rate_max = 11700000,
		.frequency_stepsize = 166667,
		.type = FE_QAM,
		.caps = FE_CAN_QAM_16 | FE_CAN_QAM_32 | FE_CAN_QAM_64 |
			FE_CAN_QAM_128 | FE_CAN_QAM_256 | FE_CAN_FEC_AUTO |
			FE_CAN_FEC_1_2 | FE_CAN_FEC_2_3 | FE_CAN_FEC_3_4 |
			FE_CAN_FEC_5_6 | FE_CAN_FEC_7_8 | FE_CAN_MUTE_TS |
			FE_CAN_TRANSMISSION_MODE_AUTO | FE_CAN_RECOVER |
			FE_CAN_GUARD_INTERVAL_AUTO | FE_CAN_HIERARCHY_AUTO
	},
	.release              = mxl251_release,
	.init                 = NULL,
	.sleep                = NULL,
	.i2c_gate_ctrl        = NULL,
	.set_frontend         = NULL,
	.get_tune_settings    = NULL,
	.read_status          = NULL,
	.read_signal_strength = NULL,
	.read_snr             = NULL,
	.read_ucblocks        = NULL,
};

static void mxl251_release(struct dvb_frontend *fe)
{
	struct mxl251_state *state = fe->tuner_priv;
	kfree(state);
}

struct dvb_frontend *mxl251_attach(struct dvb_frontend *fe,
					 struct i2c_adapter *i2c, u8 adr)
{
	struct mxl251_state *state;

	/* allocate memory for the internal state */
	state = kzalloc(sizeof(struct mxl251_state), GFP_KERNEL);
	if (state == NULL)
		goto error;

	/* setup the state */
	state->adr = adr;
	state->i2c = i2c;
	state->tuner_frequency = 0;
	state->symbol_rate = 0;
	state->fec_inner = 0;

	fe = &state->frontend;

	/* Reset / Init the device */
	fe->tuner_priv = state;
	memcpy(&fe->ops, &mxl251_ops, sizeof(struct dvb_frontend_ops));

	return fe;
error:
	kfree(state);
	return NULL;
}
EXPORT_SYMBOL_GPL(mxl251_attach);

MODULE_DESCRIPTION("MXL251 tuner/demodulator");
MODULE_AUTHOR("AVM/BCA");
MODULE_LICENSE("GPL");


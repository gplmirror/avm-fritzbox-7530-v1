#include <linux/init.h>
#include <linux/device.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>
#include <linux/clocksource.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/clk-provider.h>
#include <linux/kernel.h>
#include <linux/err.h>
#include <linux/platform_device.h>

#include <asm/mach/arch.h>
#include <clocksource/qcom-qtimer.h>

void * __iomem apcs_qtmr_base;

static const struct of_device_id qtimer_ipq40xx_match_table[] = {
	{ .compatible = "qcom,qtimer-ipq40xx" },
	{ }
};
MODULE_DEVICE_TABLE(of, qtimer_ipq40xx_match_table);

void qtimer_ipq40xx_set_next_event(qtimer_ipq40xx qtim, unsigned int cycles){
    __raw_writel(cycles, apcs_qtmr_base + (qtim * 0x1000) + APCS_QTMR_CNTP_TVAL);
}
void qtimer_ipq40xx_enable(qtimer_ipq40xx qtim, uint8_t enable, uint8_t mask_irq){
    uint32_t val = 0;
    if(enable)
        val |= 0x01;
    if(mask_irq)
        val |= 0x02;
    __raw_writel(val, apcs_qtmr_base + (qtim * 0x1000) + APCS_QTMR_CNTP_CTL);
}
static int qtimer_ipq40xx_probe(struct platform_device *pdev)
{
    struct device *dev = &pdev->dev;
    const struct of_device_id *id;
    struct resource *mem;

    id = of_match_device(qtimer_ipq40xx_match_table, dev);
    if (!id)
        return -ENODEV;
    mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
    
    apcs_qtmr_base = devm_ioremap_resource(dev, mem);
    
    if (IS_ERR(apcs_qtmr_base))
        return PTR_ERR(apcs_qtmr_base);
    return 0;
}

static int qtimer_ipq40xx_remove(struct platform_device *pdev)
{
	return 0;
}

static struct platform_driver qtimer_ipq40xx_driver = {
	.probe		= qtimer_ipq40xx_probe,
	.remove		= qtimer_ipq40xx_remove,
	.driver		= {
		.name	= "qcom,qtimer-ipq40xx",
		.owner	= THIS_MODULE,
		.of_match_table = qtimer_ipq40xx_match_table,
	},
};
static int __init qtimer_ipq40xx_init(void)
{
	return platform_driver_register(&qtimer_ipq40xx_driver);
}
core_initcall(qtimer_ipq40xx_init);
static void __exit qtimer_ipq40xx_exit(void)
{
	platform_driver_unregister(&qtimer_ipq40xx_driver);
}
module_exit(qtimer_ipq40xx_exit);

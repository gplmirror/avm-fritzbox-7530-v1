#!/usr/bin/env python3

import socket
import sys
import struct
import datetime

UDP_IP = "0.0.0.0"
UDP_PORT = 6666
BLOCK_SIZE = 1024

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

f = None
max_block = 0;

print("Wating for core files...")
while True:
    try:
        data, addr = sock.recvfrom(1500)
    except socket.timeout:
        print("done.")
        print("  size = %d" % ((max_block + 1) * BLOCK_SIZE))

        f.close()
        f = None
        max_block = 0
        sock.settimeout(None)

        continue

    if not f:
        name = 'core-%s.elf' % datetime.datetime.today().strftime('%Y-%m-%d-%H-%M-%S')
        f = open(name, 'wb')
        print('Receive "%s"...' % name, end='', flush=True)
        sock.settimeout(5)

    block_number = struct.unpack_from("!Q", data)[0]
    max_block = max(max_block, block_number)
    f.seek(block_number * BLOCK_SIZE)
    f.write(data[8:])


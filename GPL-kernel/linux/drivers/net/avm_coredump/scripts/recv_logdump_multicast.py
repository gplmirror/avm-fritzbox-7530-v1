#!/usr/bin/env python3

import socket
import sys
import struct
import datetime

UDP_IP = "ff02::4156:4d52:554c:5a43:5241:5348"
UDP_PORT = 6667

addrinfo = socket.getaddrinfo(UDP_IP, None)[0]
sock = socket.socket(addrinfo[0], socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('', UDP_PORT))

for i in range(128):
    try:
        group = socket.inet_pton(addrinfo[0], addrinfo[4][0])
        mreq = group + struct.pack('@I', i)
        sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, mreq)
    except OSError:
        break

f = None
pkgs = 0;

print("Wating for log dumps...")
while True:
    try:
        data, addr = sock.recvfrom(1500)
    except socket.timeout:
        print("done.")
        print("  pkgs = %d" % pkgs)

        f.close()
        f = None
        pkgs = 0
        sock.settimeout(None)

        continue

    if not f:
        name = 'log-%s.txt' % datetime.datetime.today().strftime('%Y-%m-%d-%H-%M-%S')
        f = open(name, 'wb')
        print('Receive "%s"...' % name, end='', flush=True)
        sock.settimeout(5)

    f.write(data)
    pkgs += 1


#!/usr/bin/env python3

import socket
import sys
import struct
import datetime

UDP_IP = "0.0.0.0"
UDP_PORT = 6667

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

f = None
pkgs = 0;

print("Wating for log dumps...")
while True:
    try:
        data, addr = sock.recvfrom(1500)
    except socket.timeout:
        print("done.")
        print("  pkgs = %d" % pkgs)

        f.close()
        f = None
        pkgs = 0
        sock.settimeout(None)

        continue

    if not f:
        name = 'log-%s.txt' % datetime.datetime.today().strftime('%Y-%m-%d-%H-%M-%S')
        f = open(name, 'wb')
        print('Receive "%s"...' % name, end='', flush=True)
        sock.settimeout(5)

    f.write(data)
    pkgs += 1


#ifndef _avm_coredump_h__
#define _avm_coredump_h_

#include <linux/types.h>
#include <asm/ptrace.h>

int avm_coredump_net_prepare_setup(void);
int avm_coredump_net_force_setup(u8* mac_addr);
int avm_coredump_net_send_eth(const void *data, unsigned int len);
//int avm_coredump_net_recv_eth(void *data, unsigned int *len);
int avm_coredump_net_poll_eth(void);
int avm_coredump_net_shutdown(void);

#endif

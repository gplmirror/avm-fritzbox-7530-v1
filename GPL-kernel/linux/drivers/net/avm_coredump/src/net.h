#ifndef __AVM_COREDUMP_NET__
#define __AVM_COREDUMP_NET__

int avm_coredump_net_init(void);
void avm_coredump_net_exit(void);

int avm_coredump_net_setup(void);
void avm_coredump_net_teardown(void);

void avm_coredump_net_prepare_pkg(u16 port, u8** buf, u32 *available_size);
int avm_coredump_net_send_pkg(u32 len);

#endif

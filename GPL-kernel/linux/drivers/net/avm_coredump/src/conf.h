#ifndef __AVM_COREDUMP_CONF__
#define __AVM_COREDUMP_CONF__

#include <linux/kernel.h>
#include <linux/socket.h>
#include <linux/kmsg_dump.h>

#define MAX_PACKET_SIZE 1500
#define CHUNK_SIZE 0x400

#define LOG_PREFIX "[avm_coredump] "

struct avm_coredump_coredump {
    // Kernel registers of the crash, set in the die notifier chain
    struct pt_regs *regs;
    // Pointer to notes array holding information about crashed cpus
    u8 *crash_notes;
    // Preallocated space for elf header
    void *elfhdr;
    unsigned long elfhdr_size;

    unsigned int num_ram_regions; //< number of ram regions
    unsigned int i_ram_region; //< currently selected ram region (for iteration)
    struct ram_region *ram_regions;

    u64 block_num;
    u64 send_bytes;
};

struct avm_coredump_logdump {
    struct kmsg_dumper dumper;
};

struct avm_coredump_conf {
    // Disallow recusive calls to avm_coredump, to avoid a crash loop when the crash code crashs
    bool crashed;

    sa_family_t	family;

    // Configuration of target that received core files
    u8  target_ip[16];
    u8  target_mac[6];

    u16 coredump_port;
    u16 logdump_port;

    // Source information. Need to be valid
    u8  source_ip[16];
    u8  source_mac[6];
    u16 source_port;


    // Information needed for the coredump functionallity
    struct avm_coredump_coredump coredump;

    // Information needed for the logdump functionallity
    struct avm_coredump_logdump logdump;

    // Tempoary memory for constructing packages
    u8 packet[MAX_PACKET_SIZE];
};

extern struct avm_coredump_conf *conf;

int avm_coredump_conf_init(void);
void avm_coredump_conf_exit(void);

#endif

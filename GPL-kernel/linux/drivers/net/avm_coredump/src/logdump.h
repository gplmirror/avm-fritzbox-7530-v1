#ifndef __AVM_COREDUMP_LOGDUMP__
#define __AVM_COREDUMP_LOGDUMP__

int avm_coredump_logdump_init(void);
void avm_coredump_logdump_exit(void);

int avm_coredump_logdump_do(void);

#endif

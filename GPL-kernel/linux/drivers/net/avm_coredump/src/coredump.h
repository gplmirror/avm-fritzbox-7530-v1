#ifndef __AVM_COREDUMP_COREDUMP__
#define __AVM_COREDUMP_COREDUMP__

int avm_coredump_coredump_init(void);
void avm_coredump_coredump_exit(void);

int avm_coredump_coredump_do(void);

#endif

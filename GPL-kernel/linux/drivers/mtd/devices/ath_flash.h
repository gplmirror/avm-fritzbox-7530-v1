#ifndef _ATH_FLASH_H
#define _ATH_FLASH_H

#define ATH_SPI_D0_HIGH		    ATH_SPI_IOC_DO	 /* Pin spi_do */
#define ATH_SPI_CLK_HIGH	    ATH_SPI_IOC_CLK	 /* Pin spi_clk */

#if 0  // [GJu] do more general
#define ATH_SPI_CS_ENABLE_0	    (AR71XX_SPI_IOC_CS1 | AR71XX_SPI_IOC_CS2)	/* Pin gpio/cs0 (active low) */
#define ATH_SPI_CS_ENABLE_1	    (AR71XX_SPI_IOC_CS0 | AR71XX_SPI_IOC_CS2)	/* Pin gpio/cs1 (active low) */
#define ATH_SPI_CS_ENABLE_2	    (AR71XX_SPI_IOC_CS0 | AR71XX_SPI_IOC_CS1)	/* Pin gpio/cs2 (active low) */
#else
#define ATH_SPI_CS_ENABLE_0	    ATH_SPI_IOC_CS_ENABLE(0)
#define ATH_SPI_CS_ENABLE_1	    ATH_SPI_IOC_CS_ENABLE(1)
#define ATH_SPI_CS_ENABLE_2	    ATH_SPI_IOC_CS_ENABLE(2)
#endif
#define ATH_SPI_CS_DIS		    ATH_SPI_IOC_CS_ALL

#define ATH_SPI_CE_LOW		(ATH_SPI_CS_ENABLE_0)
#define ATH_SPI_CE_HIGH		(ATH_SPI_CS_ENABLE_0 | ATH_SPI_CLK_HIGH)
#define ATH_SPI_SECTOR_SIZE	(1024*64)
#define ATH_SPI_PAGE_SIZE	256

#define ATH_FLASH_MAX_BANKS	1

#define ATH_SPI_CMD_WRITE_SR		0x01
#define ATH_SPI_CMD_WREN		    0x06
#define ATH_SPI_CMD_WRDI		    0x04
#define ATH_SPI_CMD_RD_STATUS		0x05
#define ATH_SPI_CMD_FAST_READ		0x0b
#define ATH_SPI_CMD_FAST_READ_4B    0x0c
#define ATH_SPI_CMD_PAGE_PROG		0x02
#define ATH_SPI_CMD_PAGE_PROG_4B	0x12
#define ATH_SPI_CMD_SECTOR_ERASE	0xd8
#define ATH_SPI_CMD_SECTOR_ERASE_4B	0xdc

/*
 * primitives
 */

#define ath_be_msb(_val, __i) (((_val) & (1 << (7 - __i))) >> (7 - __i))

#define ath_spi_bit_banger(_byte)	do {				\
	int _i;								\
	for(_i = 0; _i < 8; _i++) {					\
		ath_reg_wr_nf(ATH_SPI_WRITE, ATH_SPI_CE_LOW | ath_be_msb(_byte, _i));	\
		ath_reg_wr_nf(ATH_SPI_WRITE, ATH_SPI_CE_HIGH | ath_be_msb(_byte, _i));	\
	}								\
} while(0)

#define ath_spi_go()	do {				\
	ath_reg_wr_nf(ATH_SPI_WRITE, ATH_SPI_CE_LOW);	\
	ath_reg_wr_nf(ATH_SPI_WRITE, ATH_SPI_CS_DIS);	\
} while(0);


#define ath_spi_send_addr(__a)	do {			\
	ath_spi_bit_banger(((__a & 0xff0000) >> 16));	\
	ath_spi_bit_banger(((__a & 0x00ff00) >> 8));	\
	ath_spi_bit_banger(__a & 0x0000ff);		\
} while(0);

#define ath_spi_delay_8()	ath_spi_bit_banger(0)
#define ath_spi_done()  ath_spi_disable_soft_access()

#endif /*_ATH_FLASH_H*/

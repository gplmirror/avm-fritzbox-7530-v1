/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
#include <asm/mach_avm.h>
struct _msm_dectuart {
    struct uart_port *port;
    unsigned int mr1;
    unsigned int mr2;
    unsigned int imr;
    atomic_t     busy;

    int (*msm_dectuart_get_char)(void);
    void (*msm_dectuart_put_char)(unsigned char ch);
    void (*msm_dectuart_init)(unsigned int baud, int mode);
    void (*msm_console_stop)(void);
    void (*msm_console_start)(void);
    void (*msm_dectuart_exit)(void);
#define MSM_PIPE_SIZE   (1<<4)
#define MSM_PIPE_MASK   (MSM_PIPE_SIZE - 1)
    unsigned char pipe[MSM_PIPE_SIZE];
    volatile unsigned int read_idx, write_idx;
};

extern struct _msm_dectuart msm_dectuart;
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static inline void msm_dectuart_pipe_flush(struct _msm_dectuart *pdectuart) {
    pdectuart->read_idx = pdectuart->write_idx;
}
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static inline void msm_dectuart_pipe_enqueue(struct _msm_dectuart *pdectuart, int c) {
    unsigned int write_idx = pdectuart->write_idx;
    unsigned int post_write_idx = ((write_idx + 1) & MSM_PIPE_MASK);
    if(post_write_idx == pdectuart->read_idx) {
        return;
    }
    /*--- pr_info("%s 0x%x\n", __func__, c);  ---*/
    pdectuart->pipe[write_idx] = (unsigned char)c;
    pdectuart->write_idx = post_write_idx;
}
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
static inline int msm_dectuart_pipe_dequeue(struct _msm_dectuart *pdectuart) {
    unsigned int c;
    unsigned int read_idx = pdectuart->read_idx;
    if(read_idx == pdectuart->write_idx) {
        return -1;
    }
    c = pdectuart->pipe[read_idx];
    pdectuart->read_idx = ((read_idx + 1) & MSM_PIPE_MASK);
    return c;
}
static inline int msm_dectuart_is_busy(struct _msm_dectuart *pdectuart) {
    return atomic_read(&pdectuart->busy);
}

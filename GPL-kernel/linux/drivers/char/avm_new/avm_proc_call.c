/*--------------------------------------------------------------------------------*\
 * Ueber echo "<func>(arg0, .... arg4) [cpu<x>] [simulate]" >/proc/avm/call
 * eine beliebige Kernelfunktion ausfuehren - nur Entwicklerversion
\*--------------------------------------------------------------------------------*/
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/kallsyms.h>
#include <linux/simple_proc.h>
#include <linux/vmalloc.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/ar7wdt.h>
#include <linux/avm_event.h>
#include <linux/kthread.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) && defined(CONFIG_MIPS)
#include <linux/avm_kernel_config.h>
#endif/*--- #if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) ---*/

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) && defined(CONFIG_MIPS)
#include <asm/avm_enh/avm_enh.h>
#endif/*--- #if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) && defined(CONFIG_MIPS) ---*/

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _call_list {
	unsigned int  args[5];
	unsigned int  argcount;
	unsigned int  argstring_mask;
	unsigned int  simulate;
	unsigned int  asmlink;
	unsigned long func;
	char *tmpbuf;
};
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static char *print_callback_string(char *txt, unsigned int txt_len, struct _call_list *call) {
	char *p = txt;
	unsigned int idx = 0, len;

	len = snprintf(p, txt_len, "%pS(", (void *)call->func);
	if(txt_len >= len) txt_len -= len, p+= len;

	for(idx = 0; idx < call->argcount; idx++) {
		if(call->argstring_mask & (1 << idx)) {
			len = snprintf(p, txt_len, "%s\"%s\"", idx ? "," : "", (char *)call->args[idx]);
		} else {
			len = snprintf(p, txt_len, "%s0x%x", idx ? "," : "", call->args[idx]);
		}
		if(txt_len >= len) txt_len -= len, p+= len;
	}
	snprintf(p, txt_len, ")");
	return txt;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void call_function(struct _call_list *call) {
	unsigned int ret = 0;
	char txt[256];
	if(call->simulate) {
		printk(KERN_ERR"Call%s:%s\n", call->asmlink ? "(asmlinkage)" : "", print_callback_string(txt, sizeof(txt), call));
		kfree(call);
		return;
	}
	if(call->asmlink) {
		switch(call->argcount) {
			case 0: ret = ((asmlinkage unsigned int (*)(void))call->func)(); break;
			case 1: ret = ((asmlinkage unsigned int (*)(unsigned int))call->func)(call->args[0]); break;
			case 2: ret = ((asmlinkage unsigned int (*)(unsigned int, unsigned int))call->func)(call->args[0], call->args[1]); break;
			case 3: ret = ((asmlinkage unsigned int (*)(unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2]); break;
			case 4: ret = ((asmlinkage unsigned int (*)(unsigned int, unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2], call->args[3]); break;
			case 5: ret = ((asmlinkage unsigned int (*)(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2], call->args[3], call->args[4]); break;
		}
	} else {
		switch(call->argcount) {
			case 0: ret = ((unsigned int (*)(void))call->func)(); break;
			case 1: ret = ((unsigned int (*)(unsigned int))call->func)(call->args[0]); break;
			case 2: ret = ((unsigned int (*)(unsigned int, unsigned int))call->func)(call->args[0], call->args[1]); break;
			case 3: ret = ((unsigned int (*)(unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2]); break;
			case 4: ret = ((unsigned int (*)(unsigned int, unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2], call->args[3]); break;
			case 5: ret = ((unsigned int (*)(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int))call->func)(call->args[0], call->args[1], call->args[2], call->args[3], call->args[4]); break;
		}
	}
	printk(KERN_ERR"\n----> Called%s:%s ret=0x%08x\n", call->asmlink ? "(asmlinkage)" : "", print_callback_string(txt, sizeof(txt), call), ret);
	kfree(call);
}
#if defined(CONFIG_SMP)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void start_func_on_cpu(void *param) {
	call_function((struct _call_list *)param);
}
#endif/*--- #if defined(CONFIG_SMP) ---*/

#define SKIP_SPACES(txt) while(*txt && ((*txt == ' ') || (*txt == '\t'))) txt++
#define SKIP_UNTIL_SPACES_OR_SIGN(txt, sign) while(*txt && ((*txt != ' ') && (*txt != '\t') && (*txt != sign))) txt++
#define SKIP_UNTIL_SEPERATOR(txt) while(*txt && ((*txt != ' ') && (*txt != ',') && (*txt != '\t') && (*txt != ')'))) txt++
#define SKIP_UNTIL(txt, sign) while(*txt && (*txt != sign)) txt++

/*--------------------------------------------------------------------------------*\
 * format: (arg1, arg2, arg3 ...)
 * arg<x>  0x<val>   -> hex
 *          val      -> dezimal
 *          'string' -> string
 *          "string" -> string
 *
 * ret: NULL Fehler, sonst pointer hinter ')'
 * Beispiel:
 * echo 'strstr("test", "s" ) ' >/proc/avm/call
\*--------------------------------------------------------------------------------*/
static char *scan_arguments(char *txt, struct _call_list *call) {
	unsigned int idx = 0;
	SKIP_SPACES(txt);
	SKIP_UNTIL(txt,'(');
	if(*txt == 0) {
		printk(KERN_ERR"invalid arguments - missing '('\n");
		return NULL;
	}
	txt++;
	for(;;) {
		SKIP_SPACES(txt);
		if(txt[0] == 0) {
			printk(KERN_ERR"%s:missing ')'\n", __func__);
			return NULL;
		}
		if(txt[0] == ')') {
			txt++;
			break;
		}
		if (idx >= ARRAY_SIZE(call->args)){
			printk(KERN_ERR"%s:too much arguments\n", __func__);
			return NULL;
		}
		if(txt[0] == '0' && txt[1] == 'x') {
			sscanf(txt, "0x%x", &call->args[idx]);
		} else if(txt[0] == '\'' || txt[0] == '"') {
			unsigned int len;
			unsigned char endsign = txt[0];
			char *end;
			txt++;
			end = txt;
			SKIP_UNTIL(end, endsign);
			if(*end != endsign) {
				printk(KERN_ERR"%s:invalid arguments - missing %c\n", __func__, endsign);
				return NULL;
			}
			len  = end - txt;
			memcpy(call->tmpbuf, txt, len);
			call->args[idx] = (unsigned int)call->tmpbuf;
			call->argstring_mask |= 1 << idx;
			call->tmpbuf += len + 1;
			txt = end + 1;
		} else {
			sscanf(txt, "%d", &call->args[idx]);
		}
		idx++;
		SKIP_UNTIL_SEPERATOR(txt);
		if(*txt == ',') txt++;
	}
	call->argcount = idx;
	return txt;
}
/*--------------------------------------------------------------------------------*\
 * fuehrt eine Funktion aus:
\*--------------------------------------------------------------------------------*/
static int lproc_call(char *buffer, void *priv __attribute__((unused))) {
	struct _call_list *pcall;
	char namebuf[KSYM_NAME_LEN];
	char *p1, *p = buffer;
	unsigned int cpu, this_cpu;
	
	pcall = kzalloc(sizeof(struct _call_list) + strlen(buffer) + 1 + ARRAY_SIZE(pcall->args), GFP_KERNEL);
	if(pcall == NULL) {
		return 0;
	}
	pcall->tmpbuf = (char *)(pcall + 1);
	SKIP_SPACES(p);
	/*--- extrahiere Funktionsname/Funktionspointer: ---*/
	p1 = p;
	SKIP_UNTIL_SPACES_OR_SIGN(p1, '(');
	if(*p1) {
		size_t len = min((size_t)(p1 - p), sizeof(namebuf) - 1);
		memcpy(namebuf, p,  len);
		namebuf[len] = 0;
		pcall->func = (unsigned long)kallsyms_lookup_name(namebuf);
	}
	if(pcall->func == 0) {
		sscanf(p,"%lx", &pcall->func);
	}
	if(!func_ptr_is_kernel_text((void *)pcall->func)) {
		printk(KERN_ERR"invalid func-addr use: <funcname/addr>(arg0, .. arg4) [cpu<x>] [simulate] [asmlinkage] ('arg' for string)\n");
		kfree(pcall);
		return 0;
	}
	/*--- extrahiere Argumente: ---*/
	if((p = scan_arguments(p, pcall)) == NULL) {
		kfree(pcall);
		return 0;
	}
	/*--- nur simulieren (geparste Argumente anzeigen) ? ---*/
	if(strstr(p, "sim")) {
		pcall->simulate = 1;
	}
	if(strstr(p, "asm")) {
		pcall->asmlink = 1;
	}
	/*---ausfuehren auf CPU<x> ? ---*/
	this_cpu = get_cpu();
	if((p = strstr(p, "cpu"))) {
		p += sizeof("cpu") - 1;
		cpu = p[0] - '0';
	} else {
		cpu = this_cpu;
	}
#if defined(CONFIG_SMP)
	if(cpu_online(cpu) && (cpu != this_cpu)) {
		smp_call_function_single(cpu, start_func_on_cpu, (void *)pcall, 0);
	} else
#endif /*--- #if defined(CONFIG_SMP) ---*/
		call_function(pcall);
	put_cpu();
	return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void simulate_kernel_crash(void) {
	int *inval_pointer = NULL;

	*inval_pointer = 0x43524153;
}
/**
 */
static void event_dummy(void *context __attribute__((unused)), enum _avm_event_id id __attribute__((unused))) {
}
/**
 */
static void event_received(void *context __attribute__((unused)), unsigned char *buf __attribute__((unused)),
			   unsigned int len __attribute__((unused))) {
	msleep(10); /*--- Abarbeitung kuenstlich verzoegern ---*/
/*--- 	pr_err("%s: %u:\n", __func__, __LINE__); ---*/
};
static struct task_struct *event_kthread;
static unsigned int event_trigger_msec, event_trigger_count;
/**
 * use remote_watchdog-event (only used on puma6) to simulate event-overun or stress a little bit
 */
static int simulate_event_ovr_thread(void *data __maybe_unused) {
	void *sink_handle, *source_handle;
	struct _avm_event_id_mask id_mask;
	const struct _avm_event_remotewatchdog event = { event_header: { id: avm_event_id_remotewatchdog },
						cmd:   wdt_trigger,
						name:  "DUMMY",
						param: 120 + WDT_DEFAULT_TIME * 4,
						};
	source_handle = avm_event_source_register("avm_event_remotewatchdog",
						avm_event_build_id_mask(&id_mask, 1, avm_event_id_remotewatchdog),
						event_dummy,
						NULL);
	 sink_handle = avm_event_sink_register("avm_event_remotewatchdog_sink",
						avm_event_build_id_mask(&id_mask, 1, avm_event_id_remotewatchdog),
						event_received,
						NULL);
	if((source_handle == NULL) || (sink_handle == NULL)) {
		goto exit_thread;
	}
	while (!kthread_should_stop() && event_trigger_count) {
		struct _avm_event_remotewatchdog *pevent = (struct _avm_event_remotewatchdog *)kmalloc(
						sizeof(struct _avm_event_remotewatchdog), GFP_KERNEL);
		event_trigger_count--;
		if(pevent == NULL) {
			printk(KERN_WARNING "%s: can't alloc event\n", __func__);
			break;
		}
		memcpy(pevent, &event, sizeof(struct _avm_event_remotewatchdog));
		avm_event_source_trigger(source_handle, avm_event_id_remotewatchdog,
					sizeof(struct _avm_event_remotewatchdog), pevent);
		msleep(event_trigger_msec);
	}
exit_thread:
	if(source_handle) avm_event_source_release(source_handle);
	if(sink_handle)	 avm_event_sink_release(sink_handle);
	event_kthread = NULL;
	return 0;
}
static void **memtable;
static unsigned int free_type;
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void simulate_oom(unsigned int slab, unsigned int order, unsigned int limit) {
	unsigned int size = 1U << order;
	unsigned int i, ii;

	if(memtable) {
		i = 0;
		while(memtable[i]) {
			if(free_type) {
				kfree(memtable[i]);
			} else {
				/*--- printk(KERN_INFO"[%u]vfree(%p)\n",  i, memtable[i]); ---*/
				vfree(memtable[i]);
			}
			i++;
		}
		kfree(memtable);
		memtable = NULL;
	}
	if(limit) {
		memtable = kzalloc((limit + 1) * sizeof(void *), GFP_KERNEL);
		free_type = slab;
	} else {
		limit = (unsigned int)-1;
	}
	for(i = 0; i < limit; i++) {
		unsigned int *p;
		if(slab) {
			p = kmalloc(size, GFP_KERNEL);
			if(memtable) {
				memtable[i] = p;
			}
		} else {
			p = vmalloc(size);
			if(memtable) {
				memtable[i] = p;
				/*--- printk(KERN_INFO"[%u]vmalloc(%u) = %p\n",  i, size, memtable[i]); ---*/
			}
		}
		if(p == NULL) {
			break;
		}
		for(ii = 0; ii < size / sizeof(unsigned int); ii++) {
			p[ii] = ii ^ (unsigned int)p;
		}
	}
}
#define SIMULATE_WD "simulate_wdr"
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void simulate_wdr(void) {
#if defined(CONFIG_AVM_WATCHDOG)
	AVM_WATCHDOG_register(0, SIMULATE_WD, sizeof(SIMULATE_WD));
#endif/*--- #if defined(CONFIG_AVM_WATCHDOG) ---*/
}
#if defined(CONFIG_SMP)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void knockout_cpu(void *dummy __attribute__((unused))) {
	local_irq_disable();
	for(;;) ;
}
#endif/*--- #if defined(CONFIG_SMP) ---*/
#define SKIP_SPACES(txt) while(*txt && ((*txt == ' ') || (*txt == '\t'))) txt++
#define SKIP_UNTIL_SPACES(txt) while(*txt && ((*txt != ' ') && (*txt != '\t'))) txt++
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned long scan_addr_and_param(char *txt, unsigned int *param) {
	size_t len;
	char namebuf[KSYM_NAME_LEN];
	unsigned long addr;
	char *p;
	SKIP_SPACES(txt);
	p = txt;
	SKIP_UNTIL_SPACES(p);
	len = min((size_t)(p - txt), sizeof(namebuf) - 1);
	memcpy(namebuf, txt,  len);
	namebuf[len] = 0;
	addr = (unsigned long)kallsyms_lookup_name(namebuf);
	if(addr == 0) {
		sscanf(txt,"%lx %x", &addr, param);
	} else {
		if(*p) p++;
		SKIP_SPACES(txt);
		sscanf(p,"%x", param);
	}
	return addr;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void sim_timer_func(unsigned long context __maybe_unused) {
	int i;
	printk(KERN_ERR"%s: block timer-list for 5 s\n", __func__);
	for(i = 0; i < 5000; i++) {
		mdelay(1);
	}
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int lproc_simulate(char *buffer, void *priv __attribute__((unused))) {
	unsigned long addr = 0;
	unsigned int val = 0;
	char parsbuf[256], *p;

	strncpy(parsbuf, buffer, sizeof(parsbuf) - 1);
	parsbuf[sizeof(parsbuf) -1] = '\0';
	/*--- printk(KERN_INFO"%s parsbuf='%s'\n", __func__, parsbuf); ---*/
#if defined(print_memory_classifier)
	if((p = strstr(parsbuf, "class"))) {
		unsigned int i;
		char sym[KSYM_SYMBOL_LEN];
		p += sizeof("class") - 1;
		addr = scan_addr_and_param(p, &val);
		if(val == 0) val = 1;
		for(i = 0;i < val;i++) {
			print_memory_classifier(sym, sizeof(sym), addr, 0);
			if(sym[0] || (i == 0) || (i == (val -1))) {
				printk("%08lx --> %s\n", addr, sym);
			}
			addr += PAGE_SIZE;
		}
	} else
#endif/*--- #if defined(print_memory_classifier) ---*/
		if((strstr(parsbuf, "wdr"))) {
			printk(KERN_ERR"\nSimulate Watchdog-Reboot with '%s'\n", SIMULATE_WD);
			simulate_wdr();
	} else if((p = strstr(parsbuf, "waddr"))) {
		p += sizeof("waddr") - 1;
		addr = scan_addr_and_param(p, &val);
		if(addr < PAGE_SIZE) {
			return 0;
		}
		printk(KERN_ERR"waddr: *(%lx) = %x\n", addr, val);
		*((unsigned int *)addr) = val;
		wmb();
	} else if((p = strstr(parsbuf, "raddr"))) {
		unsigned int i, s = 3, count = 0, val;
		p += sizeof("raddr") - 1;
		addr = scan_addr_and_param(p, &count);
		if(addr == 0) {
			return 0;
		}
		if(count == 0) count = 1;
		printk(KERN_ERR"raddr: addr=%lx count=%x\n", addr, count);
		for(i = 0; i < count; i++) {
			val = *((unsigned int *)addr);
			mb();
			if(++s == 4) {
			s = 0;
			printk("\n%08lx: %08x", addr, val);
			} else {
			printk(" %08x", val);
			}
			addr += 4;
		}
		printk("\n");
	} else if((p = strstr(parsbuf, "oomslab"))) {
		unsigned int order = 0, limit= 0;
		p += sizeof("oomslab") - 1;
		SKIP_SPACES(p);
		sscanf(p,"%u %u", &order, &limit);
		if(order == 0) {
			order = 14;
		}
		printk(KERN_ERR"\nSimulate OOM per kmalloc order=%u limit=%u\n", order, limit);
		simulate_oom(1, order, limit);
	} else if((p = strstr(parsbuf, "oom"))) {
		unsigned int order = 0, limit= 0;
		p += sizeof("oom") - 1;
		SKIP_SPACES(p);
		sscanf(p,"%u %u", &order, &limit);
		if(order == 0) {
			order = 14;
		}
		printk(KERN_ERR"\nSimulate OOM per vmalloc order=%u limit=%u\n", order, limit);
		simulate_oom(0, order, limit);
	} else if((p = strstr(parsbuf, "nastytimer"))) {
		static struct timer_list sim_timer[NR_CPUS];
		void (*_sim_timer_func)(unsigned long);
		unsigned int cpu = 0;
		p += sizeof("nastytimer") - 1;
		_sim_timer_func = (void *)scan_addr_and_param(p, &cpu);
		if(!func_ptr_is_kernel_text(_sim_timer_func)) {
			_sim_timer_func = sim_timer_func;
		}
		if(cpu >= NR_CPUS) {
			cpu = 0;
		}
		printk(KERN_ERR"\nSimulate nasty-timer %pS on cpu%u in 5 seconds\n", _sim_timer_func, cpu);
		if(sim_timer[cpu].function) {
			del_timer_sync(&sim_timer[cpu]);
			sim_timer[cpu].function = NULL;
		}
		setup_timer(&sim_timer[cpu], _sim_timer_func, 0);
		sim_timer[cpu].expires = jiffies + HZ * 5;
		add_timer_on(&sim_timer[cpu], cpu);
	} else if((strstr(parsbuf, "bug_on"))) {
		int is_true = false;
		BUG_ON(is_true == false);
	} else if((p = strstr(parsbuf, "event_ovr"))) {
		p += sizeof("event_ovr") - 1;
		SKIP_SPACES(p);
		sscanf(p, "%u %u", &event_trigger_msec, &event_trigger_count);
			if(event_trigger_count == 0) {
				event_trigger_count = 100;
			}
			if(event_trigger_msec > 1000) {
				event_trigger_msec = 1000;
			}
			if(event_kthread) {
				kthread_stop(event_kthread);
			}
			event_kthread = kthread_run(simulate_event_ovr_thread, NULL, "event_ovr");
		} else if((strstr(parsbuf, "kcrash"))) {
			printk(KERN_ERR"\nSimulate Kernel-Crash\n");
			simulate_kernel_crash();
		} else if((strstr(parsbuf, "hw_wdog"))) {
#if defined(CONFIG_SMP)
			on_each_cpu(knockout_cpu, NULL, 0);
#endif/*--- #if defined(CONFIG_SMP) ---*/
			local_irq_disable();
			for(;;) ;
		} else {
			printk(KERN_INFO"unknown option use: wdr, oom, oomslab, nastytimer,"
#if defined(print_memory_classifier)
					"class <addr> <page-count>,"
#endif/*--- #if defined(print_memory_classifier) ---*/
					"kcrash, hw_wdog, waddr <addr> <val>,"
					"raddr <addr> <count>, bug_on, event_ovr <trigger_msec> <count>\n");
		}
	return 0;
}
/*--------------------------------------------------------------------------------*\
  \*--------------------------------------------------------------------------------*/
int __init avm_proc_call_init(void) {
	int add_proc = 0;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0) && defined(CONFIG_MIPS)
	if(avm_kernel_version_info) {
		int len;
		len = strlen(avm_kernel_version_info->firmwarestring);
		/*--- printk("%s: '%s' '%s'\n", __func__, avm_kernel_version_info->firmwarestring, avm_kernel_version_info->svnversion); ---*/
		if(len && avm_kernel_version_info->firmwarestring[len-1] == 'M'){
			add_proc = 1;
		}
	}
#endif
	if(add_proc) {
		add_simple_proc_file( "avm/call", lproc_call, NULL, NULL);
		add_simple_proc_file( "avm/simulate", lproc_simulate, NULL, NULL);
	}
	return 0;
}
late_initcall(avm_proc_call_init);

#include <linux/kernel.h>
#include <linux/avm_kernel_config.h>
#include <asm/prom.h>

struct _avm_kernel_config **avm_kernel_config;
struct _avm_kernel_version_info *avm_kernel_version_info;
struct _avm_kernel_urlader_env *avm_kernel_urlader_env;
struct _avm_kernel_module_memory_config *avm_kernel_module_memory_config;
unsigned char *avm_kernel_config_device_tree[avm_subrev_max];

void init_avm_kernel_config(void) {
    const char *intro = "AVM Kernel Config";
    struct _avm_kernel_config *p;

    if (init_avm_kernel_config_ptr()) {
        pr_err("[%s] %s failed\n", __func__, intro);
        return;
    }
    pr_err("[%s] %s (ptr %p)\n", __func__, intro, avm_kernel_config);

    p = *avm_kernel_config;

    avm_kernel_urlader_env = NULL;

    if (p == NULL)
        return;

    while (p->tag <= avm_kernel_config_tags_last) {
        if (p->config == NULL)
            return;

        switch (p->tag) {
            case avm_kernel_config_tags_undef:
                pr_err("[%s] %s: undef entry\n", __func__, intro);
                break;
            case avm_kernel_config_tags_module_memory:
                pr_err("[%s] %s: module memory entry\n", __func__,
                        intro);
                avm_kernel_module_memory_config =
                    (struct _avm_kernel_module_memory_config *)(p->config);
                break;
	    case avm_kernel_config_tags_version_info:
                pr_err("[%s] %s: version info entry\n", __func__,
                        intro);
                avm_kernel_version_info = (struct _avm_kernel_version_info *)(p->config);
		break;
            case avm_kernel_config_tags_last:
                return;
            case avm_kernel_config_tags_avmnet:
                pr_err("[%s] %s: unhandled avmnet entry\n", __func__,
                        intro);
                break;
            case avm_kernel_config_tags_hw_config:
                pr_err("[%s] %s: unhandled hw_config entry\n",
                        __func__, intro);
                break;
            case avm_kernel_config_tags_cache_config:
                pr_err("[%s] %s: unhandled cache_config entry\n",
                        __func__, intro);
                break;
            case avm_kernel_config_tags_urlader_env:
                pr_err("[%s] %s: Urlader environment entry\n", __func__, intro);
                avm_kernel_urlader_env = (struct _avm_kernel_urlader_env *)(p->config);
                break;
            case avm_kernel_config_tags_device_tree_subrev_0 ... avm_kernel_config_tags_device_tree_subrev_last: {
                     unsigned int index = p->tag - avm_kernel_config_tags_device_tree_subrev_0;
                     pr_err("[%s] %s: device-tree for subrev %d found\n", __func__, intro, index);
                     avm_kernel_config_device_tree[index] = (unsigned char *)((unsigned long)p->config + 0x00UL); 
                 }
                 break;
        }
        p++;
    }

    pr_err("[%s] %s: internal error, should not be reached.\n",
            __func__, intro);
}

/* vim: set noexpandtab sw=8 ts=8 sts=0: */

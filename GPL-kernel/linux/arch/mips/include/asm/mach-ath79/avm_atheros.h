#ifndef __AVM_ATHEROS_H__
#define __AVM_ATHEROS_H__
/*-----------------------------------------------------------------*\
 *--- Necessary functions from atheros.h BSP from kernel 2.6.32 ---*
\*-----------------------------------------------------------------*/
#include <generated/autoconf.h>
#include <ath79.h>
#include <ar71xx_regs.h>
#include <irq.h>
#include <asm/mach_avm.h>

/* Read and Write functions for registers */
typedef unsigned int ath_reg_t;

#define ath_reg_rd(_phys)	(*(volatile ath_reg_t *)KSEG1ADDR(_phys))

#define ath_reg_wr_nf(_phys, _val) \
	((*(volatile ath_reg_t *)KSEG1ADDR(_phys)) = (_val)); wmb() 

#define ath_reg_wr(_phys, _val) do {	\
	ath_reg_wr_nf(_phys, _val);	\
	ath_reg_rd(_phys);		\
} while(0)

#define ath_reg_rmw_set(_reg, _mask)	do {			\
	ath_reg_wr((_reg), (ath_reg_rd((_reg)) | (_mask)));	\
	ath_reg_rd((_reg));					\
} while(0)

#define ath_reg_rmw_clear(_reg, _mask) do {			\
	ath_reg_wr((_reg), (ath_reg_rd((_reg)) & ~(_mask)));	\
	ath_reg_rd((_reg));					\
} while(0)

extern void ath_spi_disable_mm(void);

/* ============================================
 * Register compatibility names,
 * TODO: Replace in avm_atheros_gpio.c!!!
 * ============================================ */

/* === GPIO-Registers === */
#define ATH_GPIO_OE                 (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_OE)
#define ATH_GPIO_IN			        (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_IN)
#define ATH_GPIO_OUT			    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_OUT)
#define ATH_GPIO_SET			    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_SET)
#define ATH_GPIO_CLEAR			    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_CLEAR)
#define ATH_GPIO_INT_ENABLE		    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_INT_MODE)
#define ATH_GPIO_INT_TYPE		    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_INT_TYPE)
#define ATH_GPIO_INT_POLARITY	    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_INT_POLARITY)
#define ATH_GPIO_INT_PENDING	    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_INT_PENDING)
#define ATH_GPIO_INT_MASK		    (AR71XX_GPIO_BASE + AR71XX_GPIO_REG_INT_ENABLE)
#define ATH_GPIO_IN_ETH_SWITCH_LED	(AR71XX_GPIO_BASE + AR71XX_GPIO_REG_FUNC)
#define ATH_GPIO_OUT_FUNCTION0		(AR71XX_GPIO_BASE + QCA955X_GPIO_REG_OUT_FUNC0)
#define ATH_GPIO_OUT_FUNCTION1		(AR71XX_GPIO_BASE + QCA955X_GPIO_REG_OUT_FUNC1)
#define ATH_GPIO_OUT_FUNCTION2		(AR71XX_GPIO_BASE + QCA955X_GPIO_REG_OUT_FUNC2)
#define ATH_GPIO_OUT_FUNCTION3		(AR71XX_GPIO_BASE + QCA955X_GPIO_REG_OUT_FUNC3)
#define ATH_GPIO_OUT_FUNCTION4		(AR71XX_GPIO_BASE + QCA955X_GPIO_REG_OUT_FUNC4)
#define ATH_GPIO_OUT_FUNCTION5		(AR71XX_GPIO_BASE + QCA955X_GPIO_REG_OUT_FUNC5)
#define ATH_GPIO_IN_ENABLE0		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x00)
#define ATH_GPIO_IN_ENABLE1		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x04)
#define ATH_GPIO_IN_ENABLE2		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x08)
#define ATH_GPIO_IN_ENABLE3		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x0c)
#define ATH_GPIO_IN_ENABLE4		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x10)
#define ATH_GPIO_IN_ENABLE5		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x14)
#define ATH_GPIO_IN_ENABLE6		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x18)
#define ATH_GPIO_IN_ENABLE7		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x1c)
#define ATH_GPIO_IN_ENABLE8		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x20)
#define ATH_GPIO_IN_ENABLE9		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_IN_ENABLE0 + 0x24)
#define ATH_GPIO_FUNCTIONS		    (AR71XX_GPIO_BASE + QCA956X_GPIO_REG_FUNC)

/* === GPIO-IRQ-Ctrl === */
#define ATH_GPIO_IRQ_BASE  ATH79_GPIO_IRQ_BASE
#define ATH_GPIO_IRQ_COUNT ATH79_GPIO_IRQ_COUNT

#if defined(NR_IRQS)
#undef NR_IRQS
#define NR_IRQS 51 + ATH_GPIO_IRQ_COUNT
#endif

/* === UART === */
#define ATH_UART_BASE           AR71XX_UART_BASE

/* High Speed Uart */
#define ATH_HS_UART_BASE        0x18500000
#define ATH_HS_UART_CS			(ATH_HS_UART_BASE + 0x04)
#define ATH_HS_UART_CLK			(ATH_HS_UART_BASE + 0x08)
#define ATH_HS_UART_INT_STATUS	(ATH_HS_UART_BASE + 0x0c)
#define ATH_HS_UART_INT_EN		(ATH_HS_UART_BASE + 0x10)

#define ATH_HS_UART_TD			0x12
#define ATH_HS_UART_RTS			0x13

#define ATH_HS_UART_TD_GPIO		23
#define ATH_HS_UART_RD_GPIO		22
#define ATH_HS_UART_RTS_GPIO	21
#define ATH_HS_UART_CTS_GPIO	18

#define ATH_HS_UART_BAUD		115200
#define ATH_HS_SERIAL_CLOCK	    100

/* === Reset Control === */
#define ATH_RESET_BASE          AR71XX_RESET_BASE
#define ATH_RESET_REG_STICKY    0xB8
#define ATH_WATCHDOG_TMR		ATH_RESET_BASE+0xc
#define ATH_WATCHDOG_TMR_CONTROL	ATH_RESET_BASE+8

#define ATH_WD_ACT_MASK			3u
#define ATH_WD_ACT_NONE			0u /* No Action */
#define ATH_WD_ACT_GP_INTR		1u /* General purpose intr */
#define ATH_WD_ACT_NMI			2u /* NMI */
#define ATH_WD_ACT_RESET		3u /* Full Chip Reset */

#ifdef CONFIG_SOC_AR71XX
#define ATH_RESET           (ATH_RESET_BASE + AR71XX_RESET_REG_RESET_MODULE)
#define ATH_RESET_FULL_CHIP AR71XX_RESET_FULL_CHIP
#define ATH_RESET_DDR       AR71XX_RESET_DDR

#define ATH_RESET_GE0_MAC   AR71XX_RESET_GE0_MAC
#define ATH_RESET_GE1_MAC   AR71XX_RESET_GE1_MAC
#endif

#ifdef CONFIG_SOC_AR724X
#define ATH_RESET           (ATH_RESET_BASE + AR724X_RESET_REG_RESET_MODULE)
#define ATH_RESET_FULL_CHIP AR724X_RESET_FULL_CHIP
#define ATH_RESET_DDR       AR724X_RESET_DDR
#endif

#ifdef CONFIG_SOC_AR913X
#define ATH_RESET           (ATH_RESET_BASE + AR913X_RESET_REG_RESET_MODULE)
#define ATH_RESET_FULL_CHIP AR913X_RESET_FULL_CHIP
#define ATH_RESET_DDR       AR913X_RESET_DDR
#endif

#ifdef CONFIG_SOC_AR933X
#define ATH_RESET           (ATH_RESET_BASE + AR933X_RESET_REG_RESET_MODULE)
#define ATH_RESET_FULL_CHIP AR933X_RESET_FULL_CHIP
#define ATH_RESET_DDR       AR933X_RESET_DDR

#define ATH_RESET_GE0_MAC   AR933X_RESET_GE0_MAC
#define ATH_RESET_GE1_MAC   AR933X_RESET_GE1_MAC
#define ATH_RESET_GE0_MDIO  AR933X_RESET_GE0_MDIO
#define ATH_RESET_GE1_MIDO  AR933X_RESET_GE1_MDIO
#endif

#ifdef CONFIG_SOC_AR934X
#define ATH_RESET           (ATH_RESET_BASE + AR934X_RESET_REG_RESET_MODULE)
#define ATH_RESET_FULL_CHIP AR934X_RESET_FULL_CHIP
#define ATH_RESET_DDR       AR934X_RESET_DDR

#define ATH_RESET_UART1     AR934X_RESET_UART1

#define ATH_RESET_GE0_MAC   AR934X_RESET_GE0_MAC
#define ATH_RESET_GE1_MAC   AR934X_RESET_GE1_MAC
#define ATH_RESET_GE0_PHY   AR934X_RESET_ETH_SWITCH
#define ATH_RESET_GE1_PHY   AR934X_RESET_ETH_SWITCH_ANALOG
#define ATH_RESET_GE0_MDIO  AR934X_RESET_GE0_MDIO
#define ATH_RESET_GE1_MDIO  AR934X_RESET_GE1_MDIO

#define RST_MISC_INTERRUPT_STATUS_ADDRESS (AR71XX_RESET_BASE + AR71XX_RESET_REG_MISC_INT_STATUS)

#endif

#ifdef CONFIG_SOC_QCA953X
#define ATH_RESET           (ATH_RESET_BASE + QCA953X_RESET_REG_RESET_MODULE)
#define ATH_RESET_FULL_CHIP QCA953X_RESET_FULL_CHIP
#define ATH_RESET_DDR       QCA953X_RESET_DDR

#define ATH_RESET_GE0_MAC   QCA953X_RESET_GE0_MAC
#define ATH_RESET_GE1_MAC   QCA953X_RESET_GE1_MAC
#define ATH_RESET_GE0_PHY   QCA953X_RESET_ETH_SWITCH
#define ATH_RESET_GE1_PHY   QCA953X_RESET_ETH_SWITCH_ANALOG
#define ATH_RESET_GE0_MDIO  QCA953X_RESET_GE0_MDIO
#define ATH_RESET_GE1_MDIO  QCA953X_RESET_GE1_MDIO
#endif

#ifdef CONFIG_SOC_QCA955X
#define ATH_RESET           (ATH_RESET_BASE + QCA955X_RESET_REG_RESET_MODULE)
#define ATH_RESET_FULL_CHIP QCA955X_RESET_FULL_CHIP
#define ATH_RESET_DDR       QCA955X_RESET_DDR
#define RST_RESET_NANDF_RESET_MASK 0x00004000

#define ATH_RESET_GE0_MAC   QCA955X_RESET_GE0_MAC
#define ATH_RESET_GE1_MAC   QCA955X_RESET_GE1_MAC
#define ATH_RESET_GE0_PHY   QCA955X_RESET_SGMII
#define ATH_RESET_GE1_PHY   QCA955X_RESET_SGMII_ANALOG
#define ATH_RESET_GE0_MDIO  QCA955X_RESET_GE0_MDIO
#define ATH_RESET_GE1_MDIO  QCA955X_RESET_GE1_MDIO
#endif

#define ATH_NAND_FLASH_BASE AR71XX_NAND_BASE

#if defined(CONFIG_SOC_QCA956X) || defined(CONFIG_SOC_QCN550X)
#define ATH_RESET               (ATH_RESET_BASE + QCA956X_RESET_REG_RESET_MODULE)
#define ATH_RESET_FULL_CHIP     QCA956X_RESET_FULL_CHIP
#define ATH_RESET_DDR           QCA956X_RESET_DDR

#define ATH_RESET_GE1_MAC		        (1 << 13)
#define ATH_RESET_GE0_MAC		        (1 << 9)
#define ATH_RESET_GE1_PHY		        (1 << 12)
#define ATH_RESET_GE0_PHY		        (1 << 8)
#define ATH_RESET_GE0_MDIO		        (1 << 22)
#define ATH_RESET_GE1_MDIO		        (1 << 23)

#undef ATH_RESET_REG_STICKY
#define ATH_RESET_REG_STICKY    0xb4

#define ATH_RESET_ETH_SWITCH_ANALOG_RESET 0x1
#define ATH_RESET_ETH_SWITCH_RESET 0x1
#endif

#if defined(CONFIG_SOC_QCN550X)
#define ATH_RESET_2             (ATH_RESET_BASE + QCN550X_RESET_REG_RESET2)
#endif

#define ATH_RST_RESET                    ATH_RESET

/* === End register name compatibility === */
#define ATH_GPIO_FUNCTION_JTAG_DISABLE  (AR724X_GPIO_FUNC_JTAG_DISABLE) 

/* === GPIO defines unused in QCA Kernel === */
#define ATH_GPIO_OUT_FUNC_REG(pin)  (ATH_GPIO_OUT_FUNCTION (0) + (pin & ~0x00000003))
#define ATH_GPIO_OUT_FUNC_SET(val, func, pin)   ((func << (8 * (pin & 0x00000003))) | (val & ~(0x000000ff << (8 * (pin & 0x00000003)))))
#define ATH_GPIO_OUT_FUNCTION(i)          (AR71XX_GPIO_BASE + AR934X_GPIO_REG_OUT_FUNC0 + (i << 2))

#define ATH_GPIO_IN_ENABLE(i)                   (AR71XX_GPIO_BASE + QCA953X_GPIO_REG_IN_ENABLE0 + (i << 2))
#define ATH_GPIO_IN_ENABLE_REG(func)            (ATH_GPIO_IN_ENABLE (0) + 0x00000004 * (func >> 8))
#define ATH_GPIO_IN_ENABLE_SET(val,func,pin)    ((pin << (func & 0xff)) | (val & ~(0xff << (func & 0xff))))

#define GPIO_IN_ENABLE_REGNUM(NUM)        (NUM << 8)
#define GPIO_IN_ENABLE_SHIFT(BITS)        (BITS)

/* === SLIC === */
#define GPIO_OUT_SLIC_DATA_OUT          3     /*--- SLIC data out ---*/
#define GPIO_OUT_SLIC_PCM_FS            4     /*--- SLIC frame sync ---*/
#define GPIO_OUT_SLIC_PCM_CLK           5     /*--- SLIC reference clock ---*/
#define GPIO_IN_SLIC_PCM_FS             (GPIO_IN_ENABLE_REGNUM(4) | GPIO_IN_ENABLE_SHIFT(8)) /*--- SLIC frame sync ---*/

/* === I2S === */
#define GPIO_OUT_I2S_CLK                13    /*--- I2S reference clock ---*/
#define GPIO_OUT_I2S_WS                 14    /*--- I2S word select for stereo ---*/
#define GPIO_OUT_I2S_SD                 15    /*--- I2S serial audio data ---*/
#define GPIO_OUT_I2S_MCK                16    /*--- I2S master clock ---*/
#define GPIO_OUT_SPDIF_OUT              17    /*--- SPDIF data output ---*/
#define GPIO_IN_I2S_MCLK                (GPIO_IN_ENABLE_REGNUM(1) | GPIO_IN_ENABLE_SHIFT(24))  /*--- I2S master clock ---*/

/* === Function Selection === */
#define ATH_GPIO_FUNCTION_STEREO_EN     AR71XX_GPIO_FUNC_STEREO_EN
#define ATH_GPIO_FUNCTION_I2S0_EN       AR933X_GPIO_FUNC_I2SO_EN
#define ATH_GPIO_FUNCTION_I2S_MCKEN     AR933X_GPIO_FUNC_I2S_MCK_EN
#define ATH_GPIO_FUNCTION_I2S_REFCLKEN  BIT(28)
#define ATH_GPIO_FUNCTION_SPDIF_EN      AR933X_GPIO_FUNC_SPDIF_EN

/* === SPI Options === */
#define ATH_SPI_FS		    (AR71XX_SPI_BASE+0x00)
#define ATH_SPI_READ		(AR71XX_SPI_BASE+0x00)
#define ATH_SPI_CLOCK		(AR71XX_SPI_BASE+0x04)
#define ATH_SPI_WRITE		(AR71XX_SPI_BASE+0x08)
#define ATH_SPI_RD_STATUS	(AR71XX_SPI_BASE+0x0c)

#define ATH_SPI_DATA_IN 	(AR71XX_SPI_BASE+0x18)
#define ATH_SPI_DATA_OUT	(AR71XX_SPI_BASE+0x10)
#define ATH_SPI_DATA_CTRL	(AR71XX_SPI_BASE+0x14)

#define ATH_SPI_CMD_WRITE_SR		0x01
#define ATH_SPI_CMD_WREN		    0x06
#define ATH_SPI_CMD_RD_STATUS		0x05
#define ATH_SPI_CMD_FAST_READ		0x0b
#define ATH_SPI_CMD_PAGE_PROG		0x02
#define ATH_SPI_CMD_SECTOR_ERASE	0xd8

/* bits */
#define QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CLK                8
#define QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS0               16
#define QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS1               17
#define QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS2               18

/* bit masks */
#define ATH_SPI_IOC_CS_ALL  \
        (BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS0) | \
         BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS1) | \
         BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS2) ) 

#define ATH_SPI_IOC_CLK \
         BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CLK)  
/* magic */
#define ATH_SPI_IOC_CS_ENABLE(i)  (ATH_SPI_IOC_CS_ALL & \
        ~BIT (QCA953X_SSB__SERIAL_FLASH_SPI__SPI_IO_CONTROL_ADDR__IO_CS0 + i))

/* ========================= */
/* ==       Ethernet      == */
/* ========================= */
#define ATH_GE0_BASE			AR71XX_GE0_BASE	/* 16M */
#define ATH_GE1_BASE			AR71XX_GE1_BASE	/* 16M */

#define ETH_CFG_ADDRESS             (AR933X_GMAC_BASE + QCA953X_GMAC_REG_ETH_CFG)
#define ETH_CFG_GE0_SGMII_SET(x)    ((x) ? QCA955X_ETH_CFG_SGMII_GMAC0 : 0x00)
#define ETH_CFG_RGMII_GE0_SET(x)    ((x) ? QCA955X_ETH_CFG_RGMII_GMAC0 : 0x00)

#define ETH_CFG_ETH_TXEN_DELAY_MSB              21
#define ETH_CFG_ETH_TXEN_DELAY_LSB              20
#define ETH_CFG_ETH_TXEN_DELAY_MASK             0x00300000
#define ETH_CFG_ETH_TXEN_DELAY_GET(x)           (((x) & ETH_CFG_ETH_TXEN_DELAY_MASK) >> ETH_CFG_ETH_TXEN_DELAY_LSB)
#define ETH_CFG_ETH_TXEN_DELAY_SET(x)           (((x) << ETH_CFG_ETH_TXEN_DELAY_LSB) & ETH_CFG_ETH_TXEN_DELAY_MASK)
#define ETH_CFG_ETH_TXEN_DELAY_RESET            0x0 // 0
#define ETH_CFG_ETH_TXD_DELAY_MSB               19
#define ETH_CFG_ETH_TXD_DELAY_LSB               18
#define ETH_CFG_ETH_TXD_DELAY_MASK              0x000c0000
#define ETH_CFG_ETH_TXD_DELAY_GET(x)            (((x) & ETH_CFG_ETH_TXD_DELAY_MASK) >> ETH_CFG_ETH_TXD_DELAY_LSB)
#define ETH_CFG_ETH_TXD_DELAY_SET(x)            (((x) << ETH_CFG_ETH_TXD_DELAY_LSB) & ETH_CFG_ETH_TXD_DELAY_MASK)
#define ETH_CFG_ETH_TXD_DELAY_RESET             0x0 // 0
#define ETH_CFG_ETH_RXDV_DELAY_MSB              17
#define ETH_CFG_ETH_RXDV_DELAY_LSB              16
#define ETH_CFG_ETH_RXDV_DELAY_MASK             0x00030000
#define ETH_CFG_ETH_RXDV_DELAY_GET(x)           (((x) & ETH_CFG_ETH_RXDV_DELAY_MASK) >> ETH_CFG_ETH_RXDV_DELAY_LSB)
#define ETH_CFG_ETH_RXDV_DELAY_SET(x)           (((x) << ETH_CFG_ETH_RXDV_DELAY_LSB) & ETH_CFG_ETH_RXDV_DELAY_MASK)
#define ETH_CFG_ETH_RXDV_DELAY_RESET            0x0 // 0
#define ETH_CFG_ETH_RXD_DELAY_MSB               15
#define ETH_CFG_ETH_RXD_DELAY_LSB               14
#define ETH_CFG_ETH_RXD_DELAY_MASK              0x0000c000
#define ETH_CFG_ETH_RXD_DELAY_GET(x)            (((x) & ETH_CFG_ETH_RXD_DELAY_MASK) >> ETH_CFG_ETH_RXD_DELAY_LSB)
#define ETH_CFG_ETH_RXD_DELAY_SET(x)            (((x) << ETH_CFG_ETH_RXD_DELAY_LSB) & ETH_CFG_ETH_RXD_DELAY_MASK)
#define ETH_CFG_ETH_RXD_DELAY_RESET             0x0 // 0

/* == PLL == */
#ifdef CONFIG_SOC_AR934X
#define ATH_PLL_SWITCH_CLOCK_CONTROL     (AR71XX_PLL_BASE + AR934X_PLL_SWITCH_CLOCK_CONTROL_REG)
#define ATH_PLL_ETH_XMII                 (AR71XX_PLL_BASE + AR934X_PLL_ETH_XMII_CONTROL_REG)

#define OTP_MEM_0_ADDRESS                       0x0000
#define OTP_INTF2_ADDRESS                       0x1008
#define OTP_STATUS0_ADDRESS                     0x1018
#define OTP_STATUS0_EFUSE_READ_DATA_VALID_MASK  0x00000004
#define OTP_STATUS1_ADDRESS                     0x101c
#define OTP_LDO_CONTROL_ADDRESS                 0x1024
#define OTP_LDO_STATUS_ADDRESS                  0x102c
#define OTP_LDO_STATUS_POWER_ON_MASK            0x00000001

#define ATH_PLL_SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MASK 0x00000f00
#endif

#ifdef CONFIG_SOC_QCA953X
#define ATH_PLL_SWITCH_CLOCK_CONTROL     (AR71XX_PLL_BASE + QCA953X_PLL_SWITCH_CLOCK_CONTROL_REG)
#define ATH_PLL_ETH_XMII                 (AR71XX_PLL_BASE + QCA953X_PLL_ETH_XMII_CONTROL_REG)

#define ATH_PLL_SWITCH_CLOCK_SPARE_MDIO_CLK_SEL1_1_SET(x)       (((x) << 14) & 0x00004000)
#define ATH_PLL_SWITCH_CLOCK_CONTROL_MDIO_CLK_SEL1_1_SET(x)     (((x) << 14) & 0x00004000)
#endif

#ifdef CONFIG_SOC_QCA955X
#define ATH_PLL_SWITCH_CLOCK_CONTROL     (AR71XX_PLL_BASE + 0x20)
#define ATH_PLL_ETH_XMII                 (AR71XX_PLL_BASE + QCA955X_PLL_ETH_XMII_CONTROL_REG)

#define OTP_MEM_0_ADDRESS                       0x18130000
#define OTP_INTF2_ADDRESS                       0x18131008
#define OTP_STATUS0_ADDRESS                     0x18131018
#define OTP_STATUS0_EFUSE_READ_DATA_VALID_MASK  0x00000004
#define OTP_STATUS1_ADDRESS                     0x1813101c
#define OTP_LDO_CONTROL_ADDRESS                 0x18131024
#define OTP_LDO_STATUS_ADDRESS                  0x1813102c
#define OTP_LDO_STATUS_POWER_ON_MASK            0x00000001

#define ETH_SGMII

#define ATH_PLL_SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MASK     0x00000f00
#define ATH_PLL_SWITCH_CLOCK_SPARE_MDIO_CLK_SEL1_1_SET(x)       (((x) << 14) & 0x00004000)
#define ATH_PLL_SWITCH_CLOCK_CONTROL_USB_REFCLK_FREQ_SEL_MASK   0x00000f00
#define ATH_PLL_SWITCH_CLOCK_CONTROL_MDIO_CLK_SEL1_1_SET(x)     (((x) << 14) & 0x00004000)

#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MSB                   11
#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_LSB                   8
#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MASK                  0x00000f00
#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_GET(x)                (((x) & SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MASK) >> SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_LSB)
#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_SET(x)                (((x) << SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_LSB) & SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MASK)
#endif

#if defined(CONFIG_SOC_QCA956X) || defined(CONFIG_SOC_QCN550X)
#define ATH_PLL_SWITCH_CLOCK_CONTROL     (AR71XX_PLL_BASE + 0x28)
#define ATH_PLL_ETH_XMII                 (AR71XX_PLL_BASE + 0x30)

#define ETH_SGMII

#define ATH_PLL_SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MASK     0x00000f00
#define ATH_PLL_SWITCH_CLOCK_SPARE_MDIO_CLK_SEL1_1_SET(x)       (((x) << 14) & 0x00004000)
#define ATH_PLL_SWITCH_CLOCK_CONTROL_USB_REFCLK_FREQ_SEL_MASK   0x00000f00
#define ATH_PLL_SWITCH_CLOCK_CONTROL_MDIO_CLK_SEL1_1_SET(x)     (((x) << 14) & 0x00004000)

#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MSB                   11
#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_LSB                   8
#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MASK                  0x00000f00
#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_GET(x)                (((x) & SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MASK) >> SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_LSB)
#define SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_SET(x)                (((x) << SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_LSB) & SWITCH_CLOCK_SPARE_USB_REFCLK_FREQ_SEL_MASK)
#endif

#if defined(ETH_SGMII)
#define SGMII_CONFIG_BERT_ENABLE_MSB                           14
#define SGMII_CONFIG_BERT_ENABLE_LSB                           14
#define SGMII_CONFIG_BERT_ENABLE_MASK                          0x00004000
#define SGMII_CONFIG_BERT_ENABLE_GET(x)                        (((x) & SGMII_CONFIG_BERT_ENABLE_MASK) >> SGMII_CONFIG_BERT_ENABLE_LSB)
#define SGMII_CONFIG_BERT_ENABLE_SET(x)                        (((x) << SGMII_CONFIG_BERT_ENABLE_LSB) & SGMII_CONFIG_BERT_ENABLE_MASK)
#define SGMII_CONFIG_BERT_ENABLE_RESET                         0x0 // 0
#define SGMII_CONFIG_PRBS_ENABLE_MSB                           13
#define SGMII_CONFIG_PRBS_ENABLE_LSB                           13
#define SGMII_CONFIG_PRBS_ENABLE_MASK                          0x00002000
#define SGMII_CONFIG_PRBS_ENABLE_GET(x)                        (((x) & SGMII_CONFIG_PRBS_ENABLE_MASK) >> SGMII_CONFIG_PRBS_ENABLE_LSB)
#define SGMII_CONFIG_PRBS_ENABLE_SET(x)                        (((x) << SGMII_CONFIG_PRBS_ENABLE_LSB) & SGMII_CONFIG_PRBS_ENABLE_MASK)
#define SGMII_CONFIG_PRBS_ENABLE_RESET                         0x0 // 0
#define SGMII_CONFIG_MDIO_COMPLETE_MSB                         12
#define SGMII_CONFIG_MDIO_COMPLETE_LSB                         12
#define SGMII_CONFIG_MDIO_COMPLETE_MASK                        0x00001000
#define SGMII_CONFIG_MDIO_COMPLETE_GET(x)                      (((x) & SGMII_CONFIG_MDIO_COMPLETE_MASK) >> SGMII_CONFIG_MDIO_COMPLETE_LSB)
#define SGMII_CONFIG_MDIO_COMPLETE_SET(x)                      (((x) << SGMII_CONFIG_MDIO_COMPLETE_LSB) & SGMII_CONFIG_MDIO_COMPLETE_MASK)
#define SGMII_CONFIG_MDIO_COMPLETE_RESET                       0x0 // 0
#define SGMII_CONFIG_MDIO_PULSE_MSB                            11
#define SGMII_CONFIG_MDIO_PULSE_LSB                            11
#define SGMII_CONFIG_MDIO_PULSE_MASK                           0x00000800
#define SGMII_CONFIG_MDIO_PULSE_GET(x)                         (((x) & SGMII_CONFIG_MDIO_PULSE_MASK) >> SGMII_CONFIG_MDIO_PULSE_LSB)
#define SGMII_CONFIG_MDIO_PULSE_SET(x)                         (((x) << SGMII_CONFIG_MDIO_PULSE_LSB) & SGMII_CONFIG_MDIO_PULSE_MASK)
#define SGMII_CONFIG_MDIO_PULSE_RESET                          0x0 // 0
#define SGMII_CONFIG_MDIO_ENABLE_MSB                           10
#define SGMII_CONFIG_MDIO_ENABLE_LSB                           10
#define SGMII_CONFIG_MDIO_ENABLE_MASK                          0x00000400
#define SGMII_CONFIG_MDIO_ENABLE_GET(x)                        (((x) & SGMII_CONFIG_MDIO_ENABLE_MASK) >> SGMII_CONFIG_MDIO_ENABLE_LSB)
#define SGMII_CONFIG_MDIO_ENABLE_SET(x)                        (((x) << SGMII_CONFIG_MDIO_ENABLE_LSB) & SGMII_CONFIG_MDIO_ENABLE_MASK)
#define SGMII_CONFIG_MDIO_ENABLE_RESET                         0x0 // 0
#define SGMII_CONFIG_NEXT_PAGE_LOADED_MSB                      9
#define SGMII_CONFIG_NEXT_PAGE_LOADED_LSB                      9
#define SGMII_CONFIG_NEXT_PAGE_LOADED_MASK                     0x00000200
#define SGMII_CONFIG_NEXT_PAGE_LOADED_GET(x)                   (((x) & SGMII_CONFIG_NEXT_PAGE_LOADED_MASK) >> SGMII_CONFIG_NEXT_PAGE_LOADED_LSB)
#define SGMII_CONFIG_NEXT_PAGE_LOADED_SET(x)                   (((x) << SGMII_CONFIG_NEXT_PAGE_LOADED_LSB) & SGMII_CONFIG_NEXT_PAGE_LOADED_MASK)
#define SGMII_CONFIG_NEXT_PAGE_LOADED_RESET                    0x0 // 0
#define SGMII_CONFIG_REMOTE_PHY_LOOPBACK_MSB                   8
#define SGMII_CONFIG_REMOTE_PHY_LOOPBACK_LSB                   8
#define SGMII_CONFIG_REMOTE_PHY_LOOPBACK_MASK                  0x00000100
#define SGMII_CONFIG_REMOTE_PHY_LOOPBACK_GET(x)                (((x) & SGMII_CONFIG_REMOTE_PHY_LOOPBACK_MASK) >> SGMII_CONFIG_REMOTE_PHY_LOOPBACK_LSB)
#define SGMII_CONFIG_REMOTE_PHY_LOOPBACK_SET(x)                (((x) << SGMII_CONFIG_REMOTE_PHY_LOOPBACK_LSB) & SGMII_CONFIG_REMOTE_PHY_LOOPBACK_MASK)
#define SGMII_CONFIG_REMOTE_PHY_LOOPBACK_RESET                 0x0 // 0
#define SGMII_CONFIG_SPEED_MSB                                 7
#define SGMII_CONFIG_SPEED_LSB                                 6
#define SGMII_CONFIG_SPEED_MASK                                0x000000c0
#define SGMII_CONFIG_SPEED_GET(x)                              (((x) & SGMII_CONFIG_SPEED_MASK) >> SGMII_CONFIG_SPEED_LSB)
#define SGMII_CONFIG_SPEED_SET(x)                              (((x) << SGMII_CONFIG_SPEED_LSB) & SGMII_CONFIG_SPEED_MASK)
#define SGMII_CONFIG_SPEED_RESET                               0x0 // 0
#define SGMII_CONFIG_FORCE_SPEED_MSB                           5
#define SGMII_CONFIG_FORCE_SPEED_LSB                           5
#define SGMII_CONFIG_FORCE_SPEED_MASK                          0x00000020
#define SGMII_CONFIG_FORCE_SPEED_GET(x)                        (((x) & SGMII_CONFIG_FORCE_SPEED_MASK) >> SGMII_CONFIG_FORCE_SPEED_LSB)
#define SGMII_CONFIG_FORCE_SPEED_SET(x)                        (((x) << SGMII_CONFIG_FORCE_SPEED_LSB) & SGMII_CONFIG_FORCE_SPEED_MASK)
#define SGMII_CONFIG_FORCE_SPEED_RESET                         0x0 // 0
#define SGMII_CONFIG_MR_REG4_CHANGED_MSB                       4
#define SGMII_CONFIG_MR_REG4_CHANGED_LSB                       4
#define SGMII_CONFIG_MR_REG4_CHANGED_MASK                      0x00000010
#define SGMII_CONFIG_MR_REG4_CHANGED_GET(x)                    (((x) & SGMII_CONFIG_MR_REG4_CHANGED_MASK) >> SGMII_CONFIG_MR_REG4_CHANGED_LSB)
#define SGMII_CONFIG_MR_REG4_CHANGED_SET(x)                    (((x) << SGMII_CONFIG_MR_REG4_CHANGED_LSB) & SGMII_CONFIG_MR_REG4_CHANGED_MASK)
#define SGMII_CONFIG_MR_REG4_CHANGED_RESET                     0x0 // 0
#define SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_MSB                 3
#define SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_LSB                 3
#define SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_MASK                0x00000008
#define SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_GET(x)              (((x) & SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_MASK) >> SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_LSB)
#define SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_SET(x)              (((x) << SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_LSB) & SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_MASK)
#define SGMII_CONFIG_ENABLE_SGMII_TX_PAUSE_RESET               0x0 // 0
#define SGMII_CONFIG_MODE_CTRL_MSB                             2
#define SGMII_CONFIG_MODE_CTRL_LSB                             0
#define SGMII_CONFIG_MODE_CTRL_MASK                            0x00000007
#define SGMII_CONFIG_MODE_CTRL_GET(x)                          (((x) & SGMII_CONFIG_MODE_CTRL_MASK) >> SGMII_CONFIG_MODE_CTRL_LSB)
#define SGMII_CONFIG_MODE_CTRL_SET(x)                          (((x) << SGMII_CONFIG_MODE_CTRL_LSB) & SGMII_CONFIG_MODE_CTRL_MASK)
#define SGMII_CONFIG_MODE_CTRL_RESET                           0x0 // 0
#define SGMII_CONFIG_ADDRESS                                   0x18070034

#define MR_AN_CONTROL_PHY_RESET_MSB                            15
#define MR_AN_CONTROL_PHY_RESET_LSB                            15
#define MR_AN_CONTROL_PHY_RESET_MASK                           0x00008000
#define MR_AN_CONTROL_PHY_RESET_GET(x)                         (((x) & MR_AN_CONTROL_PHY_RESET_MASK) >> MR_AN_CONTROL_PHY_RESET_LSB)
#define MR_AN_CONTROL_PHY_RESET_SET(x)                         (((x) << MR_AN_CONTROL_PHY_RESET_LSB) & MR_AN_CONTROL_PHY_RESET_MASK)
#define MR_AN_CONTROL_PHY_RESET_RESET                          0x0 // 0
#define MR_AN_CONTROL_LOOPBACK_MSB                             14
#define MR_AN_CONTROL_LOOPBACK_LSB                             14
#define MR_AN_CONTROL_LOOPBACK_MASK                            0x00004000
#define MR_AN_CONTROL_LOOPBACK_GET(x)                          (((x) & MR_AN_CONTROL_LOOPBACK_MASK) >> MR_AN_CONTROL_LOOPBACK_LSB)
#define MR_AN_CONTROL_LOOPBACK_SET(x)                          (((x) << MR_AN_CONTROL_LOOPBACK_LSB) & MR_AN_CONTROL_LOOPBACK_MASK)
#define MR_AN_CONTROL_LOOPBACK_RESET                           0x0 // 0
#define MR_AN_CONTROL_SPEED_SEL0_MSB                           13
#define MR_AN_CONTROL_SPEED_SEL0_LSB                           13
#define MR_AN_CONTROL_SPEED_SEL0_MASK                          0x00002000
#define MR_AN_CONTROL_SPEED_SEL0_GET(x)                        (((x) & MR_AN_CONTROL_SPEED_SEL0_MASK) >> MR_AN_CONTROL_SPEED_SEL0_LSB)
#define MR_AN_CONTROL_SPEED_SEL0_SET(x)                        (((x) << MR_AN_CONTROL_SPEED_SEL0_LSB) & MR_AN_CONTROL_SPEED_SEL0_MASK)
#define MR_AN_CONTROL_SPEED_SEL0_RESET                         0x0 // 0
#define MR_AN_CONTROL_AN_ENABLE_MSB                            12
#define MR_AN_CONTROL_AN_ENABLE_LSB                            12
#define MR_AN_CONTROL_AN_ENABLE_MASK                           0x00001000
#define MR_AN_CONTROL_AN_ENABLE_GET(x)                         (((x) & MR_AN_CONTROL_AN_ENABLE_MASK) >> MR_AN_CONTROL_AN_ENABLE_LSB)
#define MR_AN_CONTROL_AN_ENABLE_SET(x)                         (((x) << MR_AN_CONTROL_AN_ENABLE_LSB) & MR_AN_CONTROL_AN_ENABLE_MASK)
#define MR_AN_CONTROL_AN_ENABLE_RESET                          0x1 // 1
#define MR_AN_CONTROL_POWER_DOWN_MSB                           11
#define MR_AN_CONTROL_POWER_DOWN_LSB                           11
#define MR_AN_CONTROL_POWER_DOWN_MASK                          0x00000800
#define MR_AN_CONTROL_POWER_DOWN_GET(x)                        (((x) & MR_AN_CONTROL_POWER_DOWN_MASK) >> MR_AN_CONTROL_POWER_DOWN_LSB)
#define MR_AN_CONTROL_POWER_DOWN_SET(x)                        (((x) << MR_AN_CONTROL_POWER_DOWN_LSB) & MR_AN_CONTROL_POWER_DOWN_MASK)
#define MR_AN_CONTROL_POWER_DOWN_RESET                         0x0 // 0
#define MR_AN_CONTROL_RESTART_AN_MSB                           9
#define MR_AN_CONTROL_RESTART_AN_LSB                           9
#define MR_AN_CONTROL_RESTART_AN_MASK                          0x00000200
#define MR_AN_CONTROL_RESTART_AN_GET(x)                        (((x) & MR_AN_CONTROL_RESTART_AN_MASK) >> MR_AN_CONTROL_RESTART_AN_LSB)
#define MR_AN_CONTROL_RESTART_AN_SET(x)                        (((x) << MR_AN_CONTROL_RESTART_AN_LSB) & MR_AN_CONTROL_RESTART_AN_MASK)
#define MR_AN_CONTROL_RESTART_AN_RESET                         0x0 // 0
#define MR_AN_CONTROL_DUPLEX_MODE_MSB                          8
#define MR_AN_CONTROL_DUPLEX_MODE_LSB                          8
#define MR_AN_CONTROL_DUPLEX_MODE_MASK                         0x00000100
#define MR_AN_CONTROL_DUPLEX_MODE_GET(x)                       (((x) & MR_AN_CONTROL_DUPLEX_MODE_MASK) >> MR_AN_CONTROL_DUPLEX_MODE_LSB)
#define MR_AN_CONTROL_DUPLEX_MODE_SET(x)                       (((x) << MR_AN_CONTROL_DUPLEX_MODE_LSB) & MR_AN_CONTROL_DUPLEX_MODE_MASK)
#define MR_AN_CONTROL_DUPLEX_MODE_RESET                        0x1 // 1
#define MR_AN_CONTROL_SPEED_SEL1_MSB                           6
#define MR_AN_CONTROL_SPEED_SEL1_LSB                           6
#define MR_AN_CONTROL_SPEED_SEL1_MASK                          0x00000040
#define MR_AN_CONTROL_SPEED_SEL1_GET(x)                        (((x) & MR_AN_CONTROL_SPEED_SEL1_MASK) >> MR_AN_CONTROL_SPEED_SEL1_LSB)
#define MR_AN_CONTROL_SPEED_SEL1_SET(x)                        (((x) << MR_AN_CONTROL_SPEED_SEL1_LSB) & MR_AN_CONTROL_SPEED_SEL1_MASK)
#define MR_AN_CONTROL_SPEED_SEL1_RESET                         0x1 // 1
#define MR_AN_CONTROL_ADDRESS                                  0x1807001c

#define MR_AN_STATUS_BASE_PAGE_MSB                             7
#define MR_AN_STATUS_BASE_PAGE_LSB                             7
#define MR_AN_STATUS_BASE_PAGE_MASK                            0x00000080
#define MR_AN_STATUS_BASE_PAGE_GET(x)                          (((x) & MR_AN_STATUS_BASE_PAGE_MASK) >> MR_AN_STATUS_BASE_PAGE_LSB)
#define MR_AN_STATUS_BASE_PAGE_SET(x)                          (((x) << MR_AN_STATUS_BASE_PAGE_LSB) & MR_AN_STATUS_BASE_PAGE_MASK)
#define MR_AN_STATUS_BASE_PAGE_RESET                           0x1 // 0
#define MR_AN_STATUS_NO_PREAMBLE_MSB                           6
#define MR_AN_STATUS_NO_PREAMBLE_LSB                           6
#define MR_AN_STATUS_NO_PREAMBLE_MASK                          0x00000040
#define MR_AN_STATUS_NO_PREAMBLE_GET(x)                        (((x) & MR_AN_STATUS_NO_PREAMBLE_MASK) >> MR_AN_STATUS_NO_PREAMBLE_LSB)
#define MR_AN_STATUS_NO_PREAMBLE_SET(x)                        (((x) << MR_AN_STATUS_NO_PREAMBLE_LSB) & MR_AN_STATUS_NO_PREAMBLE_MASK)
#define MR_AN_STATUS_NO_PREAMBLE_RESET                         0x1
#define MR_AN_STATUS_AN_COMPLETE_MSB                           5
#define MR_AN_STATUS_AN_COMPLETE_LSB                           5
#define MR_AN_STATUS_AN_COMPLETE_MASK                          0x00000020
#define MR_AN_STATUS_AN_COMPLETE_GET(x)                        (((x) & MR_AN_STATUS_AN_COMPLETE_MASK) >> MR_AN_STATUS_AN_COMPLETE_LSB)
#define MR_AN_STATUS_AN_COMPLETE_SET(x)                        (((x) << MR_AN_STATUS_AN_COMPLETE_LSB) & MR_AN_STATUS_AN_COMPLETE_MASK)
#define MR_AN_STATUS_AN_COMPLETE_RESET                         0x1 // 1
#define MR_AN_STATUS_REMOTE_FAULT_MSB                          4
#define MR_AN_STATUS_REMOTE_FAULT_LSB                          4
#define MR_AN_STATUS_REMOTE_FAULT_MASK                         0x00000010
#define MR_AN_STATUS_REMOTE_FAULT_GET(x)                       (((x) & MR_AN_STATUS_REMOTE_FAULT_MASK) >> MR_AN_STATUS_REMOTE_FAULT_LSB)
#define MR_AN_STATUS_REMOTE_FAULT_SET(x)                       (((x) << MR_AN_STATUS_REMOTE_FAULT_LSB) & MR_AN_STATUS_REMOTE_FAULT_MASK)
#define MR_AN_STATUS_REMOTE_FAULT_RESET                        0x1
#define MR_AN_STATUS_AN_ABILITY_MSB                            3
#define MR_AN_STATUS_AN_ABILITY_LSB                            3
#define MR_AN_STATUS_AN_ABILITY_MASK                           0x00000008
#define MR_AN_STATUS_AN_ABILITY_GET(x)                         (((x) & MR_AN_STATUS_AN_ABILITY_MASK) >> MR_AN_STATUS_AN_ABILITY_LSB)
#define MR_AN_STATUS_AN_ABILITY_SET(x)                         (((x) << MR_AN_STATUS_AN_ABILITY_LSB) & MR_AN_STATUS_AN_ABILITY_MASK)
#define MR_AN_STATUS_AN_ABILITY_RESET                          0x1
#define MR_AN_STATUS_LINK_UP_MSB                               2
#define MR_AN_STATUS_LINK_UP_LSB                               2
#define MR_AN_STATUS_LINK_UP_MASK                              0x00000004
#define MR_AN_STATUS_LINK_UP_GET(x)                            (((x) & MR_AN_STATUS_LINK_UP_MASK) >> MR_AN_STATUS_LINK_UP_LSB)
#define MR_AN_STATUS_LINK_UP_SET(x)                            (((x) << MR_AN_STATUS_LINK_UP_LSB) & MR_AN_STATUS_LINK_UP_MASK)
#define MR_AN_STATUS_LINK_UP_RESET                             0x1 // 1
#define MR_AN_STATUS_EXT_CAP_MSB                               0
#define MR_AN_STATUS_EXT_CAP_LSB                               0
#define MR_AN_STATUS_EXT_CAP_MASK                              0x00000001
#define MR_AN_STATUS_EXT_CAP_GET(x)                            (((x) & MR_AN_STATUS_EXT_CAP_MASK) >> MR_AN_STATUS_EXT_CAP_LSB)
#define MR_AN_STATUS_EXT_CAP_SET(x)                            (((x) << MR_AN_STATUS_EXT_CAP_LSB) & MR_AN_STATUS_EXT_CAP_MASK)
#define MR_AN_STATUS_EXT_CAP_RESET                             0x1 // 1
#define MR_AN_STATUS_ADDRESS                                   0x18070020

#define SGMII_RESET_HW_RX_125M_N_MSB                           4
#define SGMII_RESET_HW_RX_125M_N_LSB                           4
#define SGMII_RESET_HW_RX_125M_N_MASK                          0x00000010
#define SGMII_RESET_HW_RX_125M_N_GET(x)                        (((x) & SGMII_RESET_HW_RX_125M_N_MASK) >> SGMII_RESET_HW_RX_125M_N_LSB)
#define SGMII_RESET_HW_RX_125M_N_SET(x)                        (((x) << SGMII_RESET_HW_RX_125M_N_LSB) & SGMII_RESET_HW_RX_125M_N_MASK)
#define SGMII_RESET_HW_RX_125M_N_RESET                         0x0 // 0
#define SGMII_RESET_TX_125M_N_MSB                              3
#define SGMII_RESET_TX_125M_N_LSB                              3
#define SGMII_RESET_TX_125M_N_MASK                             0x00000008
#define SGMII_RESET_TX_125M_N_GET(x)                           (((x) & SGMII_RESET_TX_125M_N_MASK) >> SGMII_RESET_TX_125M_N_LSB)
#define SGMII_RESET_TX_125M_N_SET(x)                           (((x) << SGMII_RESET_TX_125M_N_LSB) & SGMII_RESET_TX_125M_N_MASK)
#define SGMII_RESET_TX_125M_N_RESET                            0x0 // 0
#define SGMII_RESET_RX_125M_N_MSB                              2
#define SGMII_RESET_RX_125M_N_LSB                              2
#define SGMII_RESET_RX_125M_N_MASK                             0x00000004
#define SGMII_RESET_RX_125M_N_GET(x)                           (((x) & SGMII_RESET_RX_125M_N_MASK) >> SGMII_RESET_RX_125M_N_LSB)
#define SGMII_RESET_RX_125M_N_SET(x)                           (((x) << SGMII_RESET_RX_125M_N_LSB) & SGMII_RESET_RX_125M_N_MASK)
#define SGMII_RESET_RX_125M_N_RESET                            0x0 // 0
#define SGMII_RESET_TX_CLK_N_MSB                               1
#define SGMII_RESET_TX_CLK_N_LSB                               1
#define SGMII_RESET_TX_CLK_N_MASK                              0x00000002
#define SGMII_RESET_TX_CLK_N_GET(x)                            (((x) & SGMII_RESET_TX_CLK_N_MASK) >> SGMII_RESET_TX_CLK_N_LSB)
#define SGMII_RESET_TX_CLK_N_SET(x)                            (((x) << SGMII_RESET_TX_CLK_N_LSB) & SGMII_RESET_TX_CLK_N_MASK)
#define SGMII_RESET_TX_CLK_N_RESET                             0x0 // 0
#define SGMII_RESET_RX_CLK_N_MSB                               0
#define SGMII_RESET_RX_CLK_N_LSB                               0
#define SGMII_RESET_RX_CLK_N_MASK                              0x00000001
#define SGMII_RESET_RX_CLK_N_GET(x)                            (((x) & SGMII_RESET_RX_CLK_N_MASK) >> SGMII_RESET_RX_CLK_N_LSB)
#define SGMII_RESET_RX_CLK_N_SET(x)                            (((x) << SGMII_RESET_RX_CLK_N_LSB) & SGMII_RESET_RX_CLK_N_MASK)
#define SGMII_RESET_RX_CLK_N_RESET                             0x0 // 0
#define SGMII_RESET_ADDRESS                                    0x18070014

#define SGMII_MAC_RX_CONFIG_LINK_MSB                           15
#define SGMII_MAC_RX_CONFIG_LINK_LSB                           15
#define SGMII_MAC_RX_CONFIG_LINK_MASK                          0x00008000
#define SGMII_MAC_RX_CONFIG_LINK_GET(x)                        (((x) & SGMII_MAC_RX_CONFIG_LINK_MASK) >> SGMII_MAC_RX_CONFIG_LINK_LSB)
#define SGMII_MAC_RX_CONFIG_LINK_SET(x)                        (((x) << SGMII_MAC_RX_CONFIG_LINK_LSB) & SGMII_MAC_RX_CONFIG_LINK_MASK)
#define SGMII_MAC_RX_CONFIG_LINK_RESET                         0x0 // 0
#define SGMII_MAC_RX_CONFIG_ACK_MSB                            14
#define SGMII_MAC_RX_CONFIG_ACK_LSB                            14
#define SGMII_MAC_RX_CONFIG_ACK_MASK                           0x00004000
#define SGMII_MAC_RX_CONFIG_ACK_GET(x)                         (((x) & SGMII_MAC_RX_CONFIG_ACK_MASK) >> SGMII_MAC_RX_CONFIG_ACK_LSB)
#define SGMII_MAC_RX_CONFIG_ACK_SET(x)                         (((x) << SGMII_MAC_RX_CONFIG_ACK_LSB) & SGMII_MAC_RX_CONFIG_ACK_MASK)
#define SGMII_MAC_RX_CONFIG_ACK_RESET                          0x0 // 0
#define SGMII_MAC_RX_CONFIG_DUPLEX_MODE_MSB                    12
#define SGMII_MAC_RX_CONFIG_DUPLEX_MODE_LSB                    12
#define SGMII_MAC_RX_CONFIG_DUPLEX_MODE_MASK                   0x00001000
#define SGMII_MAC_RX_CONFIG_DUPLEX_MODE_GET(x)                 (((x) & SGMII_MAC_RX_CONFIG_DUPLEX_MODE_MASK) >> SGMII_MAC_RX_CONFIG_DUPLEX_MODE_LSB)
#define SGMII_MAC_RX_CONFIG_DUPLEX_MODE_SET(x)                 (((x) << SGMII_MAC_RX_CONFIG_DUPLEX_MODE_LSB) & SGMII_MAC_RX_CONFIG_DUPLEX_MODE_MASK)
#define SGMII_MAC_RX_CONFIG_DUPLEX_MODE_RESET                  0x0 // 0
#define SGMII_MAC_RX_CONFIG_SPEED_MODE_MSB                     11
#define SGMII_MAC_RX_CONFIG_SPEED_MODE_LSB                     10
#define SGMII_MAC_RX_CONFIG_SPEED_MODE_MASK                    0x00000c00
#define SGMII_MAC_RX_CONFIG_SPEED_MODE_GET(x)                  (((x) & SGMII_MAC_RX_CONFIG_SPEED_MODE_MASK) >> SGMII_MAC_RX_CONFIG_SPEED_MODE_LSB)
#define SGMII_MAC_RX_CONFIG_SPEED_MODE_SET(x)                  (((x) << SGMII_MAC_RX_CONFIG_SPEED_MODE_LSB) & SGMII_MAC_RX_CONFIG_SPEED_MODE_MASK)
#define SGMII_MAC_RX_CONFIG_SPEED_MODE_RESET                   0x0 // 0
#define SGMII_MAC_RX_CONFIG_ASM_PAUSE_MSB                      8
#define SGMII_MAC_RX_CONFIG_ASM_PAUSE_LSB                      8
#define SGMII_MAC_RX_CONFIG_ASM_PAUSE_MASK                     0x00000100
#define SGMII_MAC_RX_CONFIG_ASM_PAUSE_GET(x)                   (((x) & SGMII_MAC_RX_CONFIG_ASM_PAUSE_MASK) >> SGMII_MAC_RX_CONFIG_ASM_PAUSE_LSB)
#define SGMII_MAC_RX_CONFIG_ASM_PAUSE_SET(x)                   (((x) << SGMII_MAC_RX_CONFIG_ASM_PAUSE_LSB) & SGMII_MAC_RX_CONFIG_ASM_PAUSE_MASK)
#define SGMII_MAC_RX_CONFIG_ASM_PAUSE_RESET                    0x0 // 0
#define SGMII_MAC_RX_CONFIG_PAUSE_MSB                          7
#define SGMII_MAC_RX_CONFIG_PAUSE_LSB                          7
#define SGMII_MAC_RX_CONFIG_PAUSE_MASK                         0x00000080
#define SGMII_MAC_RX_CONFIG_PAUSE_GET(x)                       (((x) & SGMII_MAC_RX_CONFIG_PAUSE_MASK) >> SGMII_MAC_RX_CONFIG_PAUSE_LSB)
#define SGMII_MAC_RX_CONFIG_PAUSE_SET(x)                       (((x) << SGMII_MAC_RX_CONFIG_PAUSE_LSB) & SGMII_MAC_RX_CONFIG_PAUSE_MASK)
#define SGMII_MAC_RX_CONFIG_PAUSE_RESET                        0x0 // 0
#define SGMII_MAC_RX_CONFIG_RES0_MSB                           0
#define SGMII_MAC_RX_CONFIG_RES0_LSB                           0
#define SGMII_MAC_RX_CONFIG_RES0_MASK                          0x00000001
#define SGMII_MAC_RX_CONFIG_RES0_GET(x)                        (((x) & SGMII_MAC_RX_CONFIG_RES0_MASK) >> SGMII_MAC_RX_CONFIG_RES0_LSB)
#define SGMII_MAC_RX_CONFIG_RES0_SET(x)                        (((x) << SGMII_MAC_RX_CONFIG_RES0_LSB) & SGMII_MAC_RX_CONFIG_RES0_MASK)
#define SGMII_MAC_RX_CONFIG_RES0_RESET                         0x1 // 1
#define SGMII_MAC_RX_CONFIG_ADDRESS                            0x18070038

#define SGMII_RESOLVE_LINK_FAIL_CFGO_MSB                       6
#define SGMII_RESOLVE_LINK_FAIL_CFGO_LSB                       6
#define SGMII_RESOLVE_LINK_FAIL_CFGO_MASK                      0x00000040
#define SGMII_RESOLVE_LINK_FAIL_CFGO_GET(x)                    (((x) & SGMII_RESOLVE_LINK_FAIL_CFGO_MASK) >> SGMII_RESOLVE_LINK_FAIL_CFGO_LSB)
#define SGMII_RESOLVE_LINK_FAIL_CFGO_SET(x)                    (((x) << SGMII_RESOLVE_LINK_FAIL_CFGO_LSB) & SGMII_RESOLVE_LINK_FAIL_CFGO_MASK)
#define SGMII_RESOLVE_LINK_FAIL_CFGO_RESET                     0x0 // 0
#define SGMII_RESOLVE_SYNC_STATUS_MSB                          5
#define SGMII_RESOLVE_SYNC_STATUS_LSB                          5
#define SGMII_RESOLVE_SYNC_STATUS_MASK                         0x00000020
#define SGMII_RESOLVE_SYNC_STATUS_GET(x)                       (((x) & SGMII_RESOLVE_SYNC_STATUS_MASK) >> SGMII_RESOLVE_SYNC_STATUS_LSB)
#define SGMII_RESOLVE_SYNC_STATUS_SET(x)                       (((x) << SGMII_RESOLVE_SYNC_STATUS_LSB) & SGMII_RESOLVE_SYNC_STATUS_MASK)
#define SGMII_RESOLVE_SYNC_STATUS_RESET                        0x0 // 0
#define SGMII_RESOLVE_AN_SYNC_STATUS_MSB                       4
#define SGMII_RESOLVE_AN_SYNC_STATUS_LSB                       4
#define SGMII_RESOLVE_AN_SYNC_STATUS_MASK                      0x00000010
#define SGMII_RESOLVE_AN_SYNC_STATUS_GET(x)                    (((x) & SGMII_RESOLVE_AN_SYNC_STATUS_MASK) >> SGMII_RESOLVE_AN_SYNC_STATUS_LSB)
#define SGMII_RESOLVE_AN_SYNC_STATUS_SET(x)                    (((x) << SGMII_RESOLVE_AN_SYNC_STATUS_LSB) & SGMII_RESOLVE_AN_SYNC_STATUS_MASK)
#define SGMII_RESOLVE_AN_SYNC_STATUS_RESET                     0x0 // 0
#define SGMII_RESOLVE_RX_PAUSE_MSB                             3
#define SGMII_RESOLVE_RX_PAUSE_LSB                             3
#define SGMII_RESOLVE_RX_PAUSE_MASK                            0x00000008
#define SGMII_RESOLVE_RX_PAUSE_GET(x)                          (((x) & SGMII_RESOLVE_RX_PAUSE_MASK) >> SGMII_RESOLVE_RX_PAUSE_LSB)
#define SGMII_RESOLVE_RX_PAUSE_SET(x)                          (((x) << SGMII_RESOLVE_RX_PAUSE_LSB) & SGMII_RESOLVE_RX_PAUSE_MASK)
#define SGMII_RESOLVE_RX_PAUSE_RESET                           0x0 // 0
#define SGMII_RESOLVE_TX_PAUSE_MSB                             2
#define SGMII_RESOLVE_TX_PAUSE_LSB                             2
#define SGMII_RESOLVE_TX_PAUSE_MASK                            0x00000004
#define SGMII_RESOLVE_TX_PAUSE_GET(x)                          (((x) & SGMII_RESOLVE_TX_PAUSE_MASK) >> SGMII_RESOLVE_TX_PAUSE_LSB)
#define SGMII_RESOLVE_TX_PAUSE_SET(x)                          (((x) << SGMII_RESOLVE_TX_PAUSE_LSB) & SGMII_RESOLVE_TX_PAUSE_MASK)
#define SGMII_RESOLVE_TX_PAUSE_RESET                           0x0 // 0
#define SGMII_RESOLVE_DUPLEX_ERR_MSB                           1
#define SGMII_RESOLVE_DUPLEX_ERR_LSB                           1
#define SGMII_RESOLVE_DUPLEX_ERR_MASK                          0x00000001
#define SGMII_RESOLVE_DUPLEX_ERR_GET(x)                        (((x) & SGMII_RESOLVE_DUPLEX_ERR_MASK) >> SGMII_RESOLVE_DUPLEX_ERR_LSB)
#define SGMII_RESOLVE_DUPLEX_ERR_SET(x)                        (((x) << SGMII_RESOLVE_DUPLEX_ERR_LSB) & SGMII_RESOLVE_DUPLEX_ERR_MASK)
#define SGMII_RESOLVE_DUPLEX_ERR_RESET                         0x1 // 0
#define SGMII_RESOLVE_DUPLEX_MODE_MSB                          0
#define SGMII_RESOLVE_DUPLEX_MODE_LSB                          0
#define SGMII_RESOLVE_DUPLEX_MODE_MASK                         0x00000001
#define SGMII_RESOLVE_DUPLEX_MODE_GET(x)                       (((x) & SGMII_RESOLVE_DUPLEX_MODE_MASK) >> SGMII_RESOLVE_DUPLEX_MODE_LSB)
#define SGMII_RESOLVE_DUPLEX_MODE_SET(x)                       (((x) << SGMII_RESOLVE_DUPLEX_MODE_LSB) & SGMII_RESOLVE_DUPLEX_MODE_MASK)
#define SGMII_RESOLVE_DUPLEX_MODE_RESET                        0x0 // 1
#define SGMII_RESOLVE_ADDRESS                                  0x18070038

#define SGMII_DEBUG_ARB_STATE_MSB                              27
#define SGMII_DEBUG_ARB_STATE_LSB                              24
#define SGMII_DEBUG_ARB_STATE_MASK                             0x0f000000
#define SGMII_DEBUG_ARB_STATE_GET(x)                           (((x) & SGMII_DEBUG_ARB_STATE_MASK) >> SGMII_DEBUG_ARB_STATE_LSB)
#define SGMII_DEBUG_ARB_STATE_SET(x)                           (((x) << SGMII_DEBUG_ARB_STATE_LSB) & SGMII_DEBUG_ARB_STATE_MASK)
#define SGMII_DEBUG_ARB_STATE_RESET                            0x0 // 0
#define SGMII_DEBUG_RX_SYNC_STATE_MSB                          23
#define SGMII_DEBUG_RX_SYNC_STATE_LSB                          16
#define SGMII_DEBUG_RX_SYNC_STATE_MASK                         0x00ff0000
#define SGMII_DEBUG_RX_SYNC_STATE_GET(x)                       (((x) & SGMII_DEBUG_RX_SYNC_STATE_MASK) >> SGMII_DEBUG_RX_SYNC_STATE_LSB)
#define SGMII_DEBUG_RX_SYNC_STATE_SET(x)                       (((x) << SGMII_DEBUG_RX_SYNC_STATE_LSB) & SGMII_DEBUG_RX_SYNC_STATE_MASK)
#define SGMII_DEBUG_RX_SYNC_STATE_RESET                        0x0 // 0
#define SGMII_DEBUG_RX_STATE_MSB                               15
#define SGMII_DEBUG_RX_STATE_LSB                               8
#define SGMII_DEBUG_RX_STATE_MASK                              0x0000ff00
#define SGMII_DEBUG_RX_STATE_GET(x)                            (((x) & SGMII_DEBUG_RX_STATE_MASK) >> SGMII_DEBUG_RX_STATE_LSB)
#define SGMII_DEBUG_RX_STATE_SET(x)                            (((x) << SGMII_DEBUG_RX_STATE_LSB) & SGMII_DEBUG_RX_STATE_MASK)
#define SGMII_DEBUG_RX_STATE_RESET                             0x0 // 0
#define SGMII_DEBUG_TX_STATE_MSB                               7
#define SGMII_DEBUG_TX_STATE_LSB                               0
#define SGMII_DEBUG_TX_STATE_MASK                              0x000000ff
#define SGMII_DEBUG_TX_STATE_GET(x)                            (((x) & SGMII_DEBUG_TX_STATE_MASK) >> SGMII_DEBUG_TX_STATE_LSB)
#define SGMII_DEBUG_TX_STATE_SET(x)                            (((x) << SGMII_DEBUG_TX_STATE_LSB) & SGMII_DEBUG_TX_STATE_MASK)
#define SGMII_DEBUG_TX_STATE_RESET                             0x0 // 0
#define SGMII_DEBUG_ADDRESS                                    0x18070058
#define SGMII_DEBUG_OFFSET                                     0x0058

#define SGMII_INTERRUPT_INTR_MSB                               7
#define SGMII_INTERRUPT_INTR_LSB                               0
#define SGMII_INTERRUPT_INTR_MASK                              0x000000ff
#define SGMII_INTERRUPT_INTR_GET(x)                            (((x) & SGMII_INTERRUPT_INTR_MASK) >> SGMII_INTERRUPT_INTR_LSB)
#define SGMII_INTERRUPT_INTR_SET(x)                            (((x) << SGMII_INTERRUPT_INTR_LSB) & SGMII_INTERRUPT_INTR_MASK)
#define SGMII_INTERRUPT_INTR_RESET                             0x0 // 0
#define SGMII_INTERRUPT_ADDRESS                                0x1807005c
#define SGMII_INTERRUPT_OFFSET                                 0x005c
#define SGMII_INTERRUPT_SW_MASK                                0x000000ff
#define SGMII_INTERRUPT_RSTMASK                                0xffffffff
#define SGMII_INTERRUPT_RESET                                  0x00000000

#define SGMII_INTERRUPT_MASK_MASK_MSB                          7
#define SGMII_INTERRUPT_MASK_MASK_LSB                          0
#define SGMII_INTERRUPT_MASK_MASK_MASK                         0x000000ff
#define SGMII_INTERRUPT_MASK_MASK_GET(x)                       (((x) & SGMII_INTERRUPT_MASK_MASK_MASK) >> SGMII_INTERRUPT_MASK_MASK_LSB)
#define SGMII_INTERRUPT_MASK_MASK_SET(x)                       (((x) << SGMII_INTERRUPT_MASK_MASK_LSB) & SGMII_INTERRUPT_MASK_MASK_MASK)
#define SGMII_INTERRUPT_MASK_MASK_RESET                        0x0 // 0
#define SGMII_INTERRUPT_MASK_ADDRESS                           0x18070060

#define SGMII_LINK_FAIL                 (1 << 0)
#define SGMII_DUPLEX_ERR                (1 << 1)
#define SGMII_MR_AN_COMPLETE            (1 << 2)
#define SGMII_LINK_MAC_CHANGE           (1 << 3)
#define SGMII_DUPLEX_MODE_CHANGE        (1 << 4)
#define SGMII_SPEED_MODE_MAC_CHANGE     (1 << 5)
#define SGMII_RX_QUIET_CHANGE           (1 << 6)
#define SGMII_RX_MDIO_COMP_CHANGE       (1 << 7)

#if 0
#define SGMII_INTR SGMII_LINK_FAIL |\
                   SGMII_LINK_MAC_CHANGE|\
                   SGMII_DUPLEX_MODE_CHANGE |SGMII_SPEED_MODE_MAC_CHANGE
#else 
#define SGMII_INTR  (SGMII_LINK_FAIL | SGMII_LINK_FAIL | SGMII_DUPLEX_ERR | SGMII_MR_AN_COMPLETE | SGMII_LINK_MAC_CHANGE | SGMII_DUPLEX_MODE_CHANGE | SGMII_SPEED_MODE_MAC_CHANGE | SGMII_RX_QUIET_CHANGE | SGMII_RX_MDIO_COMP_CHANGE)
#endif

#define ETH_SGMII_TX_INVERT_MSB                31
#define ETH_SGMII_TX_INVERT_LSB                31
#define ETH_SGMII_TX_INVERT_MASK               0x80000000
#define ETH_SGMII_TX_INVERT_GET(x)             (((x) & ETH_SGMII_TX_INVERT_MASK) >> ETH_SGMII_TX_INVERT_LSB)
#define ETH_SGMII_TX_INVERT_SET(x)             (((x) << ETH_SGMII_TX_INVERT_LSB) & ETH_SGMII_TX_INVERT_MASK)
#define ETH_SGMII_TX_INVERT_RESET              0x0 // 0
#define ETH_SGMII_GIGE_QUAD_MSB                30
#define ETH_SGMII_GIGE_QUAD_LSB                30
#define ETH_SGMII_GIGE_QUAD_MASK               0x40000000
#define ETH_SGMII_GIGE_QUAD_GET(x)             (((x) & ETH_SGMII_GIGE_QUAD_MASK) >> ETH_SGMII_GIGE_QUAD_LSB)
#define ETH_SGMII_GIGE_QUAD_SET(x)             (((x) << ETH_SGMII_GIGE_QUAD_LSB) & ETH_SGMII_GIGE_QUAD_MASK)
#define ETH_SGMII_GIGE_QUAD_RESET              0x0 // 0
#define ETH_SGMII_RX_DELAY_MSB                 29
#define ETH_SGMII_RX_DELAY_LSB                 28
#define ETH_SGMII_RX_DELAY_MASK                0x30000000
#define ETH_SGMII_RX_DELAY_GET(x)              (((x) & ETH_SGMII_RX_DELAY_MASK) >> ETH_SGMII_RX_DELAY_LSB)
#define ETH_SGMII_RX_DELAY_SET(x)              (((x) << ETH_SGMII_RX_DELAY_LSB) & ETH_SGMII_RX_DELAY_MASK)
#define ETH_SGMII_RX_DELAY_RESET               0x0 // 0
#define ETH_SGMII_TX_DELAY_MSB                 27
#define ETH_SGMII_TX_DELAY_LSB                 26
#define ETH_SGMII_TX_DELAY_MASK                0x0c000000
#define ETH_SGMII_TX_DELAY_GET(x)              (((x) & ETH_SGMII_TX_DELAY_MASK) >> ETH_SGMII_TX_DELAY_LSB)
#define ETH_SGMII_TX_DELAY_SET(x)              (((x) << ETH_SGMII_TX_DELAY_LSB) & ETH_SGMII_TX_DELAY_MASK)
#define ETH_SGMII_TX_DELAY_RESET               0x0 // 0
#define ETH_SGMII_CLK_SEL_MSB                  25
#define ETH_SGMII_CLK_SEL_LSB                  25
#define ETH_SGMII_CLK_SEL_MASK                 0x02000000
#define ETH_SGMII_CLK_SEL_GET(x)               (((x) & ETH_SGMII_CLK_SEL_MASK) >> ETH_SGMII_CLK_SEL_LSB)
#define ETH_SGMII_CLK_SEL_SET(x)               (((x) << ETH_SGMII_CLK_SEL_LSB) & ETH_SGMII_CLK_SEL_MASK)
#define ETH_SGMII_CLK_SEL_RESET                0x1 // 1
#define ETH_SGMII_GIGE_MSB                     24
#define ETH_SGMII_GIGE_LSB                     24
#define ETH_SGMII_GIGE_MASK                    0x01000000
#define ETH_SGMII_GIGE_GET(x)                  (((x) & ETH_SGMII_GIGE_MASK) >> ETH_SGMII_GIGE_LSB)
#define ETH_SGMII_GIGE_SET(x)                  (((x) << ETH_SGMII_GIGE_LSB) & ETH_SGMII_GIGE_MASK)
#define ETH_SGMII_GIGE_RESET                   0x1 // 1
#define ETH_SGMII_PHASE1_COUNT_MSB             15
#define ETH_SGMII_PHASE1_COUNT_LSB             8
#define ETH_SGMII_PHASE1_COUNT_MASK            0x0000ff00
#define ETH_SGMII_PHASE1_COUNT_GET(x)          (((x) & ETH_SGMII_PHASE1_COUNT_MASK) >> ETH_SGMII_PHASE1_COUNT_LSB)
#define ETH_SGMII_PHASE1_COUNT_SET(x)          (((x) << ETH_SGMII_PHASE1_COUNT_LSB) & ETH_SGMII_PHASE1_COUNT_MASK)
#define ETH_SGMII_PHASE1_COUNT_RESET           0x1 // 1
#define ETH_SGMII_PHASE0_COUNT_MSB             7
#define ETH_SGMII_PHASE0_COUNT_LSB             0
#define ETH_SGMII_PHASE0_COUNT_MASK            0x000000ff
#define ETH_SGMII_PHASE0_COUNT_GET(x)          (((x) & ETH_SGMII_PHASE0_COUNT_MASK) >> ETH_SGMII_PHASE0_COUNT_LSB)
#define ETH_SGMII_PHASE0_COUNT_SET(x)          (((x) << ETH_SGMII_PHASE0_COUNT_LSB) & ETH_SGMII_PHASE0_COUNT_MASK)
#define ETH_SGMII_PHASE0_COUNT_RESET           0x1 // 1
#define ETH_SGMII_ADDRESS                      0x18050048
#define ETH_SGMII_OFFSET                       0x0048


#define SGMII_SERDES_ADDRESS                   0x18070018
#define SGMII_SERDES_VCO_REG_MSB               30
#define SGMII_SERDES_VCO_REG_LSB               27
#define SGMII_SERDES_VCO_REG_MASK              0x78000000
#define SGMII_SERDES_VCO_REG_GET(x)            (((x) & SGMII_SERDES_VCO_REG_MASK) >> SGMII_SERDES_VCO_REG_LSB)
#define SGMII_SERDES_VCO_REG_SET(x)            (((x) << SGMII_SERDES_VCO_REG_LSB) & SGMII_SERDES_VCO_REG_MASK)
#define SGMII_SERDES_VCO_REG_RESET             0x3 // 3
#define SGMII_SERDES_RES_CALIBRATION_MSB       26
#define SGMII_SERDES_RES_CALIBRATION_LSB       23
#define SGMII_SERDES_RES_CALIBRATION_MASK      0x07800000
#define SGMII_SERDES_RES_CALIBRATION_GET(x)    (((x) & SGMII_SERDES_RES_CALIBRATION_MASK) >> SGMII_SERDES_RES_CALIBRATION_LSB)
#define SGMII_SERDES_RES_CALIBRATION_SET(x)    (((x) << SGMII_SERDES_RES_CALIBRATION_LSB) & SGMII_SERDES_RES_CALIBRATION_MASK)
#define SGMII_SERDES_RES_CALIBRATION_RESET     0x0 // 0
#define SGMII_SERDES_FIBER_MODE_MSB            21
#define SGMII_SERDES_FIBER_MODE_LSB            20
#define SGMII_SERDES_FIBER_MODE_MASK           0x00300000
#define SGMII_SERDES_FIBER_MODE_GET(x)         (((x) & SGMII_SERDES_FIBER_MODE_MASK) >> SGMII_SERDES_FIBER_MODE_LSB)
#define SGMII_SERDES_FIBER_MODE_SET(x)         (((x) << SGMII_SERDES_FIBER_MODE_LSB) & SGMII_SERDES_FIBER_MODE_MASK)
#define SGMII_SERDES_FIBER_MODE_RESET          0x0 // 0
#define SGMII_SERDES_THRESHOLD_CTRL_MSB        19
#define SGMII_SERDES_THRESHOLD_CTRL_LSB        18
#define SGMII_SERDES_THRESHOLD_CTRL_MASK       0x000c0000
#define SGMII_SERDES_THRESHOLD_CTRL_GET(x)     (((x) & SGMII_SERDES_THRESHOLD_CTRL_MASK) >> SGMII_SERDES_THRESHOLD_CTRL_LSB)
#define SGMII_SERDES_THRESHOLD_CTRL_SET(x)     (((x) << SGMII_SERDES_THRESHOLD_CTRL_LSB) & SGMII_SERDES_THRESHOLD_CTRL_MASK)
#define SGMII_SERDES_THRESHOLD_CTRL_RESET      0x0 // 0
#define SGMII_SERDES_FIBER_SDO_MSB             17
#define SGMII_SERDES_FIBER_SDO_LSB             17
#define SGMII_SERDES_FIBER_SDO_MASK            0x00020000
#define SGMII_SERDES_FIBER_SDO_GET(x)          (((x) & SGMII_SERDES_FIBER_SDO_MASK) >> SGMII_SERDES_FIBER_SDO_LSB)
#define SGMII_SERDES_FIBER_SDO_SET(x)          (((x) << SGMII_SERDES_FIBER_SDO_LSB) & SGMII_SERDES_FIBER_SDO_MASK)
#define SGMII_SERDES_FIBER_SDO_RESET           0x0 // 0
#define SGMII_SERDES_EN_SIGNAL_DETECT_MSB      16
#define SGMII_SERDES_EN_SIGNAL_DETECT_LSB      16
#define SGMII_SERDES_EN_SIGNAL_DETECT_MASK     0x00010000
#define SGMII_SERDES_EN_SIGNAL_DETECT_GET(x)   (((x) & SGMII_SERDES_EN_SIGNAL_DETECT_MASK) >> SGMII_SERDES_EN_SIGNAL_DETECT_LSB)
#define SGMII_SERDES_EN_SIGNAL_DETECT_SET(x)   (((x) << SGMII_SERDES_EN_SIGNAL_DETECT_LSB) & SGMII_SERDES_EN_SIGNAL_DETECT_MASK)
#define SGMII_SERDES_EN_SIGNAL_DETECT_RESET    0x1 // 1
#define SGMII_SERDES_LOCK_DETECT_STATUS_MSB    15
#define SGMII_SERDES_LOCK_DETECT_STATUS_LSB    15
#define SGMII_SERDES_LOCK_DETECT_STATUS_MASK   0x00008000
#define SGMII_SERDES_LOCK_DETECT_STATUS_GET(x) (((x) & SGMII_SERDES_LOCK_DETECT_STATUS_MASK) >> SGMII_SERDES_LOCK_DETECT_STATUS_LSB)
#define SGMII_SERDES_LOCK_DETECT_STATUS_SET(x) (((x) << SGMII_SERDES_LOCK_DETECT_STATUS_LSB) & SGMII_SERDES_LOCK_DETECT_STATUS_MASK)
#define SGMII_SERDES_LOCK_DETECT_STATUS_RESET  0x0 // 0
#define SGMII_SERDES_SPARE0_MSB                14
#define SGMII_SERDES_SPARE0_LSB                11
#define SGMII_SERDES_SPARE0_MASK               0x00007800
#define SGMII_SERDES_SPARE0_GET(x)             (((x) & SGMII_SERDES_SPARE0_MASK) >> SGMII_SERDES_SPARE0_LSB)
#define SGMII_SERDES_SPARE0_SET(x)             (((x) << SGMII_SERDES_SPARE0_LSB) & SGMII_SERDES_SPARE0_MASK)
#define SGMII_SERDES_SPARE0_RESET              0x0 // 0
#define SGMII_SERDES_VCO_SLOW_MSB              10
#define SGMII_SERDES_VCO_SLOW_LSB              10
#define SGMII_SERDES_VCO_SLOW_MASK             0x00000400
#define SGMII_SERDES_VCO_SLOW_GET(x)           (((x) & SGMII_SERDES_VCO_SLOW_MASK) >> SGMII_SERDES_VCO_SLOW_LSB)
#define SGMII_SERDES_VCO_SLOW_SET(x)           (((x) << SGMII_SERDES_VCO_SLOW_LSB) & SGMII_SERDES_VCO_SLOW_MASK)
#define SGMII_SERDES_VCO_SLOW_RESET            0x0 // 0
#define SGMII_SERDES_VCO_FAST_MSB              9
#define SGMII_SERDES_VCO_FAST_LSB              9
#define SGMII_SERDES_VCO_FAST_MASK             0x00000200
#define SGMII_SERDES_VCO_FAST_GET(x)           (((x) & SGMII_SERDES_VCO_FAST_MASK) >> SGMII_SERDES_VCO_FAST_LSB)
#define SGMII_SERDES_VCO_FAST_SET(x)           (((x) << SGMII_SERDES_VCO_FAST_LSB) & SGMII_SERDES_VCO_FAST_MASK)
#define SGMII_SERDES_VCO_FAST_RESET            0x0 // 0
#define SGMII_SERDES_PLL_BW_MSB                8
#define SGMII_SERDES_PLL_BW_LSB                8
#define SGMII_SERDES_PLL_BW_MASK               0x00000100
#define SGMII_SERDES_PLL_BW_GET(x)             (((x) & SGMII_SERDES_PLL_BW_MASK) >> SGMII_SERDES_PLL_BW_LSB)
#define SGMII_SERDES_PLL_BW_SET(x)             (((x) << SGMII_SERDES_PLL_BW_LSB) & SGMII_SERDES_PLL_BW_MASK)
#define SGMII_SERDES_PLL_BW_RESET              0x1 // 1
#define SGMII_SERDES_TX_IMPEDANCE_MSB          7
#define SGMII_SERDES_TX_IMPEDANCE_LSB          7
#define SGMII_SERDES_TX_IMPEDANCE_MASK         0x00000080
#define SGMII_SERDES_TX_IMPEDANCE_GET(x)       (((x) & SGMII_SERDES_TX_IMPEDANCE_MASK) >> SGMII_SERDES_TX_IMPEDANCE_LSB)
#define SGMII_SERDES_TX_IMPEDANCE_SET(x)       (((x) << SGMII_SERDES_TX_IMPEDANCE_LSB) & SGMII_SERDES_TX_IMPEDANCE_MASK)
#define SGMII_SERDES_TX_IMPEDANCE_RESET        0x0 // 0
#define SGMII_SERDES_TX_DR_CTRL_MSB            6
#define SGMII_SERDES_TX_DR_CTRL_LSB            4
#define SGMII_SERDES_TX_DR_CTRL_MASK           0x00000070
#define SGMII_SERDES_TX_DR_CTRL_GET(x)         (((x) & SGMII_SERDES_TX_DR_CTRL_MASK) >> SGMII_SERDES_TX_DR_CTRL_LSB)
#define SGMII_SERDES_TX_DR_CTRL_SET(x)         (((x) << SGMII_SERDES_TX_DR_CTRL_LSB) & SGMII_SERDES_TX_DR_CTRL_MASK)
#define SGMII_SERDES_TX_DR_CTRL_RESET          0x1 // 1
#define SGMII_SERDES_HALF_TX_MSB               3
#define SGMII_SERDES_HALF_TX_LSB               3
#define SGMII_SERDES_HALF_TX_MASK              0x00000008
#define SGMII_SERDES_HALF_TX_GET(x)            (((x) & SGMII_SERDES_HALF_TX_MASK) >> SGMII_SERDES_HALF_TX_LSB)
#define SGMII_SERDES_HALF_TX_SET(x)            (((x) << SGMII_SERDES_HALF_TX_LSB) & SGMII_SERDES_HALF_TX_MASK)
#define SGMII_SERDES_HALF_TX_RESET             0x0 // 0
#define SGMII_SERDES_CDR_BW_MSB                2
#define SGMII_SERDES_CDR_BW_LSB                1
#define SGMII_SERDES_CDR_BW_MASK               0x00000006
#define SGMII_SERDES_CDR_BW_GET(x)             (((x) & SGMII_SERDES_CDR_BW_MASK) >> SGMII_SERDES_CDR_BW_LSB)
#define SGMII_SERDES_CDR_BW_SET(x)             (((x) << SGMII_SERDES_CDR_BW_LSB) & SGMII_SERDES_CDR_BW_MASK)
#define SGMII_SERDES_CDR_BW_RESET              0x3 // 3
#define SGMII_SERDES_RX_IMPEDANCE_MSB          0
#define SGMII_SERDES_RX_IMPEDANCE_LSB          0
#define SGMII_SERDES_RX_IMPEDANCE_MASK         0x00000001
#define SGMII_SERDES_RX_IMPEDANCE_GET(x)       (((x) & SGMII_SERDES_RX_IMPEDANCE_MASK) >> SGMII_SERDES_RX_IMPEDANCE_LSB)
#define SGMII_SERDES_RX_IMPEDANCE_SET(x)       (((x) << SGMII_SERDES_RX_IMPEDANCE_LSB) & SGMII_SERDES_RX_IMPEDANCE_MASK)
#define SGMII_SERDES_RX_IMPEDANCE_RESET        0x0 // 0

#define ETH_SGMII_SERDES_EN_LOCK_DETECT_MSB     2
#define ETH_SGMII_SERDES_EN_LOCK_DETECT_LSB     2
#define ETH_SGMII_SERDES_EN_LOCK_DETECT_MASK    0x00000004
#define ETH_SGMII_SERDES_EN_LOCK_DETECT_GET(x)  (((x) & ETH_SGMII_SERDES_EN_LOCK_DETECT_MASK) >> ETH_SGMII_SERDES_EN_LOCK_DETECT_LSB)
#define ETH_SGMII_SERDES_EN_LOCK_DETECT_SET(x)  (((x) << ETH_SGMII_SERDES_EN_LOCK_DETECT_LSB) & ETH_SGMII_SERDES_EN_LOCK_DETECT_MASK)
#define ETH_SGMII_SERDES_EN_LOCK_DETECT_RESET   0x0 // 0
#define ETH_SGMII_SERDES_PLL_REFCLK_SEL_MSB     1
#define ETH_SGMII_SERDES_PLL_REFCLK_SEL_LSB     1
#define ETH_SGMII_SERDES_PLL_REFCLK_SEL_MASK    0x00000002
#define ETH_SGMII_SERDES_PLL_REFCLK_SEL_GET(x)  (((x) & ETH_SGMII_SERDES_PLL_REFCLK_SEL_MASK) >> ETH_SGMII_SERDES_PLL_REFCLK_SEL_LSB)
#define ETH_SGMII_SERDES_PLL_REFCLK_SEL_SET(x)  (((x) << ETH_SGMII_SERDES_PLL_REFCLK_SEL_LSB) & ETH_SGMII_SERDES_PLL_REFCLK_SEL_MASK)
#define ETH_SGMII_SERDES_PLL_REFCLK_SEL_RESET   0x0 // 0
#define ETH_SGMII_SERDES_EN_PLL_MSB             0
#define ETH_SGMII_SERDES_EN_PLL_LSB             0
#define ETH_SGMII_SERDES_EN_PLL_MASK            0x00000001
#define ETH_SGMII_SERDES_EN_PLL_GET(x)          (((x) & ETH_SGMII_SERDES_EN_PLL_MASK) >> ETH_SGMII_SERDES_EN_PLL_LSB)
#define ETH_SGMII_SERDES_EN_PLL_SET(x)          (((x) << ETH_SGMII_SERDES_EN_PLL_LSB) & ETH_SGMII_SERDES_EN_PLL_MASK)
#define ETH_SGMII_SERDES_EN_PLL_RESET           0x1 // 1
#define ETH_SGMII_SERDES_ADDRESS                (AR71XX_PLL_BASE + 0x4c)
#define ETH_SGMII_SERDES_EN_LOCK_DETECT_MASK    0x00000004

#define ATH_PLL_ETH_SGMII_PHASE0_COUNT_SET(x)   (((x) << 0) & 0x000000ff)
#define ATH_PLL_ETH_SGMII_PHASE1_COUNT_SET(x)   (((x) << 8) & 0x0000ff00)

#define ATH_PLL_ETH_SGMII_SERDES_PLL_REFCLK_SEL_SET ETH_SGMII_SERDES_PLL_REFCLK_SEL_SET
#define ATH_PLL_ETH_SGMII_SERDES_EN_LOCK_DETECT_SET ETH_SGMII_SERDES_EN_LOCK_DETECT_SET
#endif

#define ETH_XMII_TX_INVERT_MSB                  31
#define ETH_XMII_TX_INVERT_LSB                  31
#define ETH_XMII_TX_INVERT_MASK                 0x80000000
#define ETH_XMII_TX_INVERT_GET(x)               (((x) & ETH_XMII_TX_INVERT_MASK) >> ETH_XMII_TX_INVERT_LSB)
#define ETH_XMII_TX_INVERT_SET(x)               (((x) << ETH_XMII_TX_INVERT_LSB) & ETH_XMII_TX_INVERT_MASK)
#define ETH_XMII_TX_INVERT_RESET                0
#define ETH_XMII_GIGE_QUAD_MSB                  30
#define ETH_XMII_GIGE_QUAD_LSB                  30
#define ETH_XMII_GIGE_QUAD_MASK                 0x40000000
#define ETH_XMII_GIGE_QUAD_GET(x)               (((x) & ETH_XMII_GIGE_QUAD_MASK) >> ETH_XMII_GIGE_QUAD_LSB)
#define ETH_XMII_GIGE_QUAD_SET(x)               (((x) << ETH_XMII_GIGE_QUAD_LSB) & ETH_XMII_GIGE_QUAD_MASK)
#define ETH_XMII_GIGE_QUAD_RESET                0
#define ETH_XMII_RX_DELAY_MSB                   29
#define ETH_XMII_RX_DELAY_LSB                   28
#define ETH_XMII_RX_DELAY_MASK                  0x30000000
#define ETH_XMII_RX_DELAY_GET(x)                (((x) & ETH_XMII_RX_DELAY_MASK) >> ETH_XMII_RX_DELAY_LSB)
#define ETH_XMII_RX_DELAY_SET(x)                (((x) << ETH_XMII_RX_DELAY_LSB) & ETH_XMII_RX_DELAY_MASK)
#define ETH_XMII_RX_DELAY_RESET                 0
#define ETH_XMII_TX_DELAY_MSB                   27
#define ETH_XMII_TX_DELAY_LSB                   26
#define ETH_XMII_TX_DELAY_MASK                  0x0c000000
#define ETH_XMII_TX_DELAY_GET(x)                (((x) & ETH_XMII_TX_DELAY_MASK) >> ETH_XMII_TX_DELAY_LSB)
#define ETH_XMII_TX_DELAY_SET(x)                (((x) << ETH_XMII_TX_DELAY_LSB) & ETH_XMII_TX_DELAY_MASK)
#define ETH_XMII_TX_DELAY_RESET                 0
#define ETH_XMII_GIGE_MSB                       25
#define ETH_XMII_GIGE_LSB                       25
#define ETH_XMII_GIGE_MASK                      0x02000000
#define ETH_XMII_GIGE_GET(x)                    (((x) & ETH_XMII_GIGE_MASK) >> ETH_XMII_GIGE_LSB)
#define ETH_XMII_GIGE_SET(x)                    (((x) << ETH_XMII_GIGE_LSB) & ETH_XMII_GIGE_MASK)
#define ETH_XMII_GIGE_RESET                     0
#define ETH_XMII_OFFSET_PHASE_MSB               24
#define ETH_XMII_OFFSET_PHASE_LSB               24
#define ETH_XMII_OFFSET_PHASE_MASK              0x01000000
#define ETH_XMII_OFFSET_PHASE_GET(x)            (((x) & ETH_XMII_OFFSET_PHASE_MASK) >> ETH_XMII_OFFSET_PHASE_LSB)
#define ETH_XMII_OFFSET_PHASE_SET(x)            (((x) << ETH_XMII_OFFSET_PHASE_LSB) & ETH_XMII_OFFSET_PHASE_MASK)
#define ETH_XMII_OFFSET_PHASE_RESET             0
#define ETH_XMII_OFFSET_COUNT_MSB               23
#define ETH_XMII_OFFSET_COUNT_LSB               16
#define ETH_XMII_OFFSET_COUNT_MASK              0x00ff0000
#define ETH_XMII_OFFSET_COUNT_GET(x)            (((x) & ETH_XMII_OFFSET_COUNT_MASK) >> ETH_XMII_OFFSET_COUNT_LSB)
#define ETH_XMII_OFFSET_COUNT_SET(x)            (((x) << ETH_XMII_OFFSET_COUNT_LSB) & ETH_XMII_OFFSET_COUNT_MASK)
#define ETH_XMII_OFFSET_COUNT_RESET             0
#define ETH_XMII_PHASE1_COUNT_MSB               15
#define ETH_XMII_PHASE1_COUNT_LSB               8
#define ETH_XMII_PHASE1_COUNT_MASK              0x0000ff00
#define ETH_XMII_PHASE1_COUNT_GET(x)            (((x) & ETH_XMII_PHASE1_COUNT_MASK) >> ETH_XMII_PHASE1_COUNT_LSB)
#define ETH_XMII_PHASE1_COUNT_SET(x)            (((x) << ETH_XMII_PHASE1_COUNT_LSB) & ETH_XMII_PHASE1_COUNT_MASK)
#define ETH_XMII_PHASE1_COUNT_RESET             1
#define ETH_XMII_PHASE0_COUNT_MSB               7
#define ETH_XMII_PHASE0_COUNT_LSB               0
#define ETH_XMII_PHASE0_COUNT_MASK              0x000000ff
#define ETH_XMII_PHASE0_COUNT_GET(x)            (((x) & ETH_XMII_PHASE0_COUNT_MASK) >> ETH_XMII_PHASE0_COUNT_LSB)
#define ETH_XMII_PHASE0_COUNT_SET(x)            (((x) << ETH_XMII_PHASE0_COUNT_LSB) & ETH_XMII_PHASE0_COUNT_MASK)
#define ETH_XMII_PHASE0_COUNT_RESET             1
#define ETH_XMII_ADDRESS                        0x18050028
#define ETH_XMII_OFFSET                         0x0028
#define ATH_PLL_ETH_SGMII                       (AR71XX_PLL_BASE + QCA955X_PLL_ETH_SGMII_CONTROL_REG)
#define ATH_PLL_ETH_SGMII_SERDES                (AR71XX_PLL_BASE + 0x4c)
#define ATH_PLL_ETH_SGMII_GIGE_SET(x)           ((x) ? BIT(24) : 0x00)
#define ATH_PLL_ETH_SGMII_CLK_SEL_SET(x)        ((x) ? BIT(25) : 0x00)

#define ATH_PLL_ETH_XMII_TX_DELAY_SET           ETH_XMII_TX_DELAY_SET
#define ATH_PLL_ETH_XMII_RX_DELAY_SET           ETH_XMII_RX_DELAY_SET

#define ATH_PLL_ETH_XMII_TX_INVERT_SET          ETH_XMII_TX_INVERT_SET
#define ATH_PLL_ETH_XMII_GIGE_SET               ETH_XMII_GIGE_SET
#define ATH_PLL_ETH_XMII_PHASE0_COUNT_SET       ETH_XMII_PHASE0_COUNT_SET
#define ATH_PLL_ETH_XMII_PHASE1_COUNT_SET       ETH_XMII_PHASE1_COUNT_SET

#if defined(CONFIG_SOC_QCA956X)
#define ETH_CFG_SW_PHY_SWAP_LSB                                      8
#define ETH_CFG_SW_PHY_SWAP_MASK                                     0x00000100
#define ETH_CFG_SW_PHY_SWAP_SET(x)                                   (((x) << ETH_CFG_SW_PHY_SWAP_LSB) & ETH_CFG_SW_PHY_SWAP_MASK)

#define ETH_CFG_SW_ONLY_MODE_LSB                                     7
#define ETH_CFG_SW_ONLY_MODE_MASK                                    0x00000080
#define ETH_CFG_SW_ONLY_MODE_SET(x)                                  (((x) << ETH_CFG_SW_ONLY_MODE_LSB) & ETH_CFG_SW_ONLY_MODE_MASK)

#elif defined(CONFIG_SOC_QCA953X)

#define ETH_CFG_SW_PHY_SWAP_LSB                                      7
#define ETH_CFG_SW_PHY_SWAP_MASK                                     0x00000080
#define ETH_CFG_SW_PHY_SWAP_SET(x)                                   (((x) << ETH_CFG_SW_PHY_SWAP_LSB) & ETH_CFG_SW_PHY_SWAP_MASK)
#endif

#define SWITCH_CLOCK_SPARE_OEN_CLK125M_PLL_LSB                       18
#define SWITCH_CLOCK_SPARE_OEN_CLK125M_PLL_MASK                      0x00040000
#define SWITCH_CLOCK_SPARE_OEN_CLK125M_PLL_SET(x)                    (((x) << SWITCH_CLOCK_SPARE_OEN_CLK125M_PLL_LSB) & SWITCH_CLOCK_SPARE_OEN_CLK125M_PLL_MASK)


#define SWITCH_CLOCK_SPARE_EN_PLL_TOP_LSB                            12
#define SWITCH_CLOCK_SPARE_EN_PLL_TOP_MASK                           0x00001000
#define SWITCH_CLOCK_SPARE_EN_PLL_TOP_SET(x)                         (((x) << SWITCH_CLOCK_SPARE_EN_PLL_TOP_LSB) & SWITCH_CLOCK_SPARE_EN_PLL_TOP_MASK)


#define SWITCH_CLOCK_SPARE_SWITCHCLK_SEL_LSB                         19
#define SWITCH_CLOCK_SPARE_SWITCHCLK_SEL_MASK                        0x00080000
#define SWITCH_CLOCK_SPARE_SWITCHCLK_SEL_SET(x)                      (((x) << SWITCH_CLOCK_SPARE_SWITCHCLK_SEL_LSB) & SWITCH_CLOCK_SPARE_SWITCHCLK_SEL_MASK)

#define ATH_PLL_SWITCH_CLOCK_CONTROL_OEN_CLK125M_SEL_SET       SWITCH_CLOCK_SPARE_OEN_CLK125M_PLL_SET
#define ATH_PLL_SWITCH_CLOCK_CONTROL_EN_PLL_TOP_SET            SWITCH_CLOCK_SPARE_EN_PLL_TOP_SET
#define ATH_PLL_SWITCH_CLOCK_CONTROL_SWITCHCLK_SEL_SET         SWITCH_CLOCK_SPARE_SWITCHCLK_SEL_SET

#define ATH_PLL_ETH_XMII_TX_DELAY_SET                          ETH_XMII_TX_DELAY_SET
#define ATH_PLL_ETH_XMII_RX_DELAY_SET                          ETH_XMII_RX_DELAY_SET

#define ATH_PLL_ETH_SGMII_RX_DELAY_SET                         ETH_SGMII_RX_DELAY_SET
#define ATH_PLL_ETH_SGMII_TX_DELAY_SET                         ETH_SGMII_TX_DELAY_SET

#define ATH_PLL_ETH_SGMII_SERDES_PLL_EN_LOCK_DETECT_SET        ETH_SGMII_SERDES_EN_LOCK_DETECT_SET
#define ATH_PLL_ETH_SGMII_SERDES_EN_LOCK_DETECT_MASK           ETH_SGMII_SERDES_EN_LOCK_DETECT_MASK
#define ATH_PLL_ETH_SGMII_SERDES_PLL_EN_MASK                   ETH_SGMII_SERDES_EN_PLL_MASK
#define ATH_PLL_ETH_SGMII_SERDES_PLL_REFCLK_SEL_SET            ETH_SGMII_SERDES_PLL_REFCLK_SEL_SET
#define ATH_PLL_ETH_SGMII_SERDES_EN_LOCK_DETECT_SET            ETH_SGMII_SERDES_EN_LOCK_DETECT_SET

#define ATH_RESET_REVISION_ID_MAJOR_MASK                       REV_ID_MAJOR_MASK

// GMAC
#define ATH_GMAC_ETH_CFG                 ETH_CFG_ADDRESS
#define ATH_GMAC_SGMII_RESET             SGMII_RESET_ADDRESS
#define ATH_GMAC_SGMII_SERDES            SGMII_SERDES_ADDRESS
#define ATH_GMAC_MR_AN_CONTROL           MR_AN_CONTROL_ADDRESS
#define ATH_GMAC_SGMII_CONFIG            SGMII_CONFIG_ADDRESS
#define ATH_GMAC_SGMII_DEBUG             SGMII_DEBUG_ADDRESS

#define ATH_GMAC_ETH_CFG_SW_PHY_SWAP_SET                       ETH_CFG_SW_PHY_SWAP_SET
#define ATH_GMAC_ETH_CFG_GE0_SGMII_SET                         ETH_CFG_GE0_SGMII_SET
#define ATH_GMAC_ETH_CFG_SW_ONLY_MODE_SET                      ETH_CFG_SW_ONLY_MODE_SET

#define ATH_GMAC_SGMII_RESET_HW_RX_125M_N_SET                  SGMII_RESET_HW_RX_125M_N_SET
#define ATH_GMAC_SGMII_RESET_TX_125M_N_SET                     SGMII_RESET_TX_125M_N_SET
#define ATH_GMAC_SGMII_RESET_RX_125M_N_SET                     SGMII_RESET_RX_125M_N_SET
#define ATH_GMAC_SGMII_RESET_TX_CLK_N_SET                      SGMII_RESET_TX_CLK_N_SET
#define ATH_GMAC_SGMII_RESET_RX_CLK_N_SET                      SGMII_RESET_RX_CLK_N_SET
#define ATH_GMAC_SGMII_RESET_RX_CLK_N_RESET                    SGMII_RESET_RX_CLK_N_RESET

#define ATH_GMAC_SGMII_SERDES_VCO_FAST_GET                     SGMII_SERDES_VCO_FAST_GET
#define ATH_GMAC_SGMII_SERDES_VCO_SLOW_GET                     SGMII_SERDES_VCO_SLOW_GET

#define ATH_GMAC_SGMII_SERDES_RES_CALIBRATION_MASK             SGMII_SERDES_RES_CALIBRATION_MASK
#define ATH_GMAC_SGMII_SERDES_RES_CALIBRATION_SET              SGMII_SERDES_RES_CALIBRATION_SET

#define ATH_GMAC_SGMII_SERDES_CDR_BW_SET                       SGMII_SERDES_CDR_BW_SET
#define ATH_GMAC_SGMII_SERDES_TX_DR_CTRL_SET                   SGMII_SERDES_TX_DR_CTRL_SET
#define ATH_GMAC_SGMII_SERDES_PLL_BW_SET                       SGMII_SERDES_PLL_BW_SET
#define ATH_GMAC_SGMII_SERDES_EN_SIGNAL_DETECT_SET             SGMII_SERDES_EN_SIGNAL_DETECT_SET

#define ATH_GMAC_SGMII_SERDES_LOCK_DETECT_STATUS_MASK          SGMII_SERDES_LOCK_DETECT_STATUS_MASK

#define ATH_GMAC_SGMII_SERDES_FIBER_SDO_SET                    SGMII_SERDES_FIBER_SDO_SET
#define ATH_GMAC_SGMII_SERDES_VCO_REG_SET                      SGMII_SERDES_VCO_REG_SET

#define ATH_GMAC_MR_AN_CONTROL_POWER_DOWN_SET                  MR_AN_CONTROL_POWER_DOWN_SET
#define ATH_GMAC_MR_AN_CONTROL_PHY_RESET_SET                   MR_AN_CONTROL_PHY_RESET_SET
#define ATH_GMAC_MR_AN_CONTROL_DUPLEX_MODE_SET                 MR_AN_CONTROL_DUPLEX_MODE_SET
#define ATH_GMAC_MR_AN_CONTROL_SPEED_SEL1_SET                  MR_AN_CONTROL_SPEED_SEL1_SET

#define ATH_GMAC_SGMII_CONFIG_MODE_CTRL_SET                    SGMII_CONFIG_MODE_CTRL_SET
#define ATH_GMAC_SGMII_CONFIG_FORCE_SPEED_SET                  SGMII_CONFIG_FORCE_SPEED_SET
#define ATH_GMAC_SGMII_CONFIG_SPEED_SET                        SGMII_CONFIG_SPEED_SET

#if defined(CONFIG_SOC_QCN550X)
#define ATH_PCIE_PHY_REG1_S_MSB                                3
#define ATH_PCIE_PHY_REG1_S_LSB                                0
#define ATH_PCIE_PHY_REG1_S_MASK                               0x0000000f
#define ATH_PCIE_PHY_REG1_S_GET(x)                             (((x) & ATH_PCIE_PHY_REG1_S_MASK) >> ATH_PCIE_PHY_REG1_S_LSB)
#define ATH_PCIE_PHY_REG1_S_SET(x)                             (((x) << ATH_PCIE_PHY_REG1_S_LSB) & ATH_PCIE_PHY_REG1_S_MASK)
#define ATH_PCIE_PHY_REG1_S_RESET                              0x7
#define ATH_PCIE_PHY_REG1 0x18116E00
#else
#define ATH_PCIE_PHY_REG1 0x18116CC0
#endif
#define ATH_PCIE_EP_PHY_REG1 0x18116D00

#endif

#define ATH_RST_CLKGAT_EN 0x180600C8
#define ATH_RST_CLKGAT_EN_PCIE_RC (1 << 2)
#define ATH_RST_CLKGAT_EN_CLK100_PCIE_RC (1 << 3)
#define ATH_RST_CLKGAT_EN_GE0 (1 << 5)
#define ATH_RST_CLKGAT_EN_GE1 (1 << 6)
#define ATH_RST_CLKGAT_EN_USB1 (1 << 7)
#define ATH_RST_CLKGAT_EN_WMAC (1 << 9)




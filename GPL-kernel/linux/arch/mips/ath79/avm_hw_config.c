
#include <linux/kernel.h>

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_current_hw_config = NULL;
EXPORT_SYMBOL(avm_current_hw_config);
/*------------------------------------------------------------------------------------------*\
  [GJu] new: fill the GPIO table defined literally before from DeviceTree now
\*------------------------------------------------------------------------------------------*/

#include <avm_atheros_gpio.h>
#include <linux/of.h>

enum _led_event { /* DUMMY DEFINITION */ LastEvent = 0 };
int (*led_event_action)(int, enum _led_event , unsigned int ) = NULL;
EXPORT_SYMBOL(led_event_action);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_get_hw_config_table(void)
{
    if(avm_current_hw_config){
        return avm_current_hw_config;
    }
    printk("[%s] No Config Table found, generation from Device Tree must have failed!\n", __FUNCTION__); 
    BUG_ON(1);
    return NULL;
}
EXPORT_SYMBOL(avm_get_hw_config_table);


int init_gpio_config(void) 
{
    struct _avm_hw_config *config;
    
    config = avm_get_hw_config_table();
    if(!config)
        return -1; // error: no hardware config found!
 
    return ath_avm_gpio_init();
}

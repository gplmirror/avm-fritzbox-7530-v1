if ATH79

menu "Atheros AR71XX/AR724X/AR913X machine selection"

config ATH79_MACH_AP121
	bool "Atheros AP121 reference board"
	select SOC_AR933X
	select ATH79_DEV_ETH
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	help
	  Say 'Y' here if you want your kernel to support the
	  Atheros AP121 reference board.

config ATH79_MACH_AVM_GENERIC
    def_bool n

config ATH79_MACH_AVM_HW200
	bool "AVM Fritz WlanRepeater 450E (HW200)"
	select SOC_QCA955X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM FritzRepeater 450E.

config ATH79_MACH_AVM_HW205
	bool "AVM Fritz WlanRepeater DVB-C (HW205)"
	select SOC_QCA955X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM FritzRepeater DVB-C.

config ATH79_MACH_AVM_HW206
	bool "AVM Fritz WlanRepeater 1750E (HW206)"
	select SOC_QCA955X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM FritzRepeater 1750E.

config ATH79_MACH_AVM_HW214
	bool "AVM Fritz!Box 6820 LTE (HW214)"
	select SOC_QCA955X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM Fritz!Box 6820 LTE.

config ATH79_MACH_AVM_HW215
	bool "AVM Fritz!Repeater 310v2"
	select SOC_QCA953X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM Fritz!Repeater 310 v2.

config ATH79_MACH_AVM_HW216
	bool "AVM Fritz!Repeater 1160"
	select SOC_QCA953X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM Fritz!Repeater 1160.

config ATH79_MACH_AVM_HW219
	bool "AVM Fritz!Box 4020 (HW219)"
	select SOC_QCA956X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM Fritz!Box 4020.

config ATH79_MACH_AVM_HW222
	bool "AVM Fritz!Powerline 1240E"
	select SOC_QCA953X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM Fritz!Powerline 1240E.

config ATH79_MACH_AVM_HW238
	bool "AVM FritzBox 7490 OffloadTarget (HW238)"
	select SOC_QCA955X
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM 7490 Offload Target.

config ATH79_MACH_AVM_HW240
	bool "AVM Fritz WlanRepeater 600 or 2400 (HW240/HW241)"
	select SOC_QCN550X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM FritzRepeater 600 or 2400.

config ATH79_MACH_AVM_WASP
	bool "AVM WASP (Repeater 310, Powerline 540E/546E)"
	select SOC_AR934X
	select ATH79_DEV_AP9X_PCI if PCI
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_NFC
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	select ATH79_MACH_AVM_GENERIC
	help
	  Say 'Y' here if you want your kernel to support the
	  AVM WASP Platforms like Repeater 310 and Powerline 540E or 546E.

config ATH79_MACH_AP135
	bool "Qualcomm Atheros AP135 reference board"
	select SOC_QCA955X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	select ATH79_DEV_ETH
	help
	  Say 'Y' here if you want your kernel to support the
	  Qualcomm Atheros AP135 reference board.

config ATH79_MACH_AP135_DUAL
	bool "Qualcomm Atheros AP135 Dual reference board"
	select SOC_QCA955X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_NAND
	select ATH79_DEV_WMAC
	help
	  Say 'Y' here if you want your kernel to support the
	  Qualcomm Atheros AP135 Dual reference board.

config ATH79_MACH_AP137
	bool "Qualcomm Atheros AP137 reference board"
	select SOC_QCA955X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	help
	  Say 'Y' here if you want your kernel to support the
	  Qualcomm Atheros AP135 reference board.

config ATH79_MACH_AP136
	bool "Atheros AP136/AP135 reference board"
	select SOC_QCA955X
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_NFC
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	help
	  Say 'Y' here if you want your kernel to support the
	  Atheros AP136 or AP135 reference boards.

config ATH79_MACH_AP81
	bool "Atheros AP81 reference board"
	select SOC_AR913X
	select ATH79_DEV_ETH
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	help
	  Say 'Y' here if you want your kernel to support the
	  Atheros AP81 reference board.

config ATH79_MACH_DB120
	bool "Atheros DB120 reference board"
	select SOC_AR934X
	select ATH79_DEV_AP9X_PCI if PCI
	select ATH79_DEV_ETH
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_NFC
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	help
	  Say 'Y' here if you want your kernel to support the
	  Atheros DB120 reference board.

config ATH79_MACH_PB44
	bool "Atheros PB44 reference board"
	select SOC_AR71XX
	select ATH79_DEV_ETH
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_SPI
	select ATH79_DEV_USB
	help
	  Say 'Y' here if you want your kernel to support the
	  Atheros PB44 reference board.

config ATH79_MACH_UBNT_XM
	bool "Ubiquiti Networks XM/UniFi boards"
	select SOC_AR724X
	select SOC_AR934X
	select ATH79_DEV_AP9X_PCI if PCI
	select ATH79_DEV_ETH
	select ATH79_DEV_GPIO_BUTTONS
	select ATH79_DEV_LEDS_GPIO
	select ATH79_DEV_M25P80
	select ATH79_DEV_USB
	select ATH79_DEV_WMAC
	help
	  Say 'Y' here if you want your kernel to support the
	  Ubiquiti Networks XM (rev 1.0) board.

choice
	prompt "Build a DTB in the kernel"
	optional
	help
	  Select a devicetree that should be built into the kernel.

	config DTB_TL_WR1043ND_V1
		bool "TL-WR1043ND Version 1"
		select BUILTIN_DTB
		select SOC_AR913X
endchoice

config AVM_PCI_RECOVERY
	bool "AVM PCI Recovery on link down"
	depends on PCI_AR724X
	help
	  Enables the reset of the root complex once a link error
	  has been detected.

source "arch/mips/ath79/Kconfig.openwrt"

endmenu

config SOC_AR71XX
	select HW_HAS_PCI
	def_bool n

config SOC_AR724X
	select HW_HAS_PCI
	select PCI_AR724X if PCI
	def_bool n

config SOC_AR913X
	def_bool n

config SOC_AR933X
	def_bool n

config SOC_AR934X
	select HW_HAS_PCI
	select PCI_AR724X if PCI
    select CPU_HAS_DSP_ASE
	def_bool n

config SOC_QCA953X
	select USB_ARCH_HAS_EHCI
	select HW_HAS_PCI
	select PCI_AR724X if PCI
	def_bool n

config SOC_QCA955X
	select HW_HAS_PCI
	select PCI_AR724X if PCI
    select CPU_HAS_DSP_ASE
	def_bool n

config SOC_QCA956X
	select USB_ARCH_HAS_EHCI
	select HW_HAS_PCI
	select PCI_AR724X if PCI
    select CPU_HAS_DSP_ASE
	def_bool n

config SOC_QCN550X
	select USB_ARCH_HAS_EHCI
	select HW_HAS_PCI
	select PCI_AR724X if PCI
	def_bool n

config ATH79_DEV_M25P80
	select ATH79_DEV_SPI
	def_bool n

config ATH79_DEV_AP9X_PCI
	select ATH79_PCI_ATH9K_FIXUP
	def_bool n

config ATH79_DEV_DSA
	def_bool n

config ATH79_DEV_ETH
	def_bool n

config PCI_AR724X
	def_bool n

config ATH79_DEV_GPIO_BUTTONS
	def_bool n

config ATH79_DEV_LEDS_GPIO
	def_bool n

config ATH79_DEV_NFC
	depends on (SOC_AR934X || SOC_QCA955X)
	def_bool n

config ATH79_DEV_NAND
	select ATH79_NAND_CALDATA_FIXUP
	def_bool n

config ATH79_DEV_SPI
	def_bool n

config ATH79_DEV_USB
	def_bool n

config ATH79_DEV_WMAC
	depends on (SOC_AR913X || SOC_AR933X || SOC_AR934X || SOC_QCA953X || SOC_QCA955X || SOC_QCA956X || SOC_QCN550X)
	def_bool n

config ATH79_NVRAM
	def_bool n

config ATH79_PCI_ATH9K_FIXUP
	def_bool n

config ATH79_ROUTERBOOT
	def_bool n

endif

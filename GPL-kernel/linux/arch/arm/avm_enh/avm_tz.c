 /*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/init_task.h>
#include <mach/avm_gic_smc.h>
#include <linux/qcom_scm.h>

#ifdef CONFIG_AVM_FASTIRQ_TZ

void setup_avm_fiq_tz(void *dummy __maybe_unused){
    register u32 r0 asm("r0") = AVM_SMC_FIQ_MODE;
    register u32 r1 asm("r1") = 0;
    asm volatile(
            ".arch_extension sec\n"
            "smc	#0	@ switch to secure world\n"
            : 
            : "r" (r0), "r" (r1) 
            : "r2", "r3", "cc");
	pr_err("Changed SCR FIQ Bit\n");
}

#endif



#define AVM_TZ_MAGIC 0x41564d00

uint32_t avm_get_tz_version(uint32_t *version, uint32_t *modified){
    register u32 r0 asm("r0") = AVM_SMC_GET_VERSION;
    register u32 r1 asm("r1") = 0;
    register u32 r2 asm("r2") = 0;
    uint32_t magic;
    asm volatile(
			__asmeq("%0", "r0")
			__asmeq("%1", "r1")
			__asmeq("%2", "r2")
			__asmeq("%3", "r0")
            ".arch_extension sec\n"
            "smc	#0	@ switch to secure world\n"
            : "=r" (r0), "=r" (r1), "=r" (r2)
            : "r" (r0) 
            : "r3", "cc");
    magic = r0;
    if(magic == 0x41564d00){
        *version = r1;
        *modified = r2;
        return 0;
    }
    else{
        return -1;
    }
}
uint32_t avm_is_avm_tz(void){
    static uint32_t version = 0;
    static uint32_t modified = 0;
    static uint32_t already_queried = 0;
    if(already_queried == 0){
        already_queried = 1;
        if(avm_get_tz_version(&version, &modified) != 0){
            version = 0;
        }
    }
    if(version > 0)
        return 1;
    return 0;
}
void avm_secure_wdt_pet(void){
    qcom_scm_avm_wdt_pet();
}
void avm_secure_wdt_config(u32 enable, u32 bark, u32 bite){
    qcom_scm_avm_wdt_config(enable, bark, bite);
}

#if defined(CONFIG_PROC_FS)
#include <linux/simple_proc.h>

void tz_proc_stat(struct seq_file *m, void *data __maybe_unused) {
    uint32_t version, modified;
    if(avm_get_tz_version(&version, &modified)){
        seq_printf(m, "0\n");
        return;
    }
    seq_printf(m, "%d%s", version, (modified ? "M\n" : "\n"));
	return;
}


int avm_tz_proc_setup(void){
    add_simple_proc_file("avm/tz_version", NULL, tz_proc_stat, NULL);
    return 0;
}
late_initcall(avm_tz_proc_setup);
#endif

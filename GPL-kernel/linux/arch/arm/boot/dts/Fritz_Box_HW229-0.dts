/* Copyright (c) 2015, The Linux Foundation. All rights reserved.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include "Fritz_qcom-ipq4019.dtsi"
#include "../../../../include/linux/avm_hw_config_def.h"

/ {
	model = "AVM FRITZ!Powerline 1260E";
	compatible = "qcom,ipq4019";

    avm-hw-revision{
        compatible = "avm,avm_hw_revision";
        revision = "229";
        subrevision = "1";
    };

    soc {
		#address-cells = <1>;
		#size-cells = <1>;
        mdio@90000 {
            #address-cells = <1>;
            #size-cells = <0>;
            phy0: ethernet-phy@0 {
                reg = <0>;
            };
            phy1: ethernet-phy@1 {
                reg = <1>;
            };
            phy2: ethernet-phy@2 {
                reg = <2>;
            };
            phy3: ethernet-phy@3 {
                reg = <3>;
            };
            phy4: ethernet-phy@4 {
                reg = <4>;
            };
        };

        edma: edma@c080000 {
                qcom,num-gmac = <2>;
                
                gmac0 {
                    qcom,phy-mdio-addr = <3>;
                    qcom,poll-required = <1>;
                    qcom,forced-speed = <0>;
                    qcom,forced-duplex = <0>;
                    vlan-tag = <0 0x10>;
                    devname = "eth0";
                    macname = "maca";
                    mac = <0>;
                };
                gmac1 {
                    qcom,phy-mdio_addr = <4>;
                    qcom,poll-required = <1>;
                    qcom,forced-speed = <0>;
                    qcom,forced-duplex = <0>;
                    vlan-tag = <0 0x20>;
                    devname = "plc";
                    macname = "usb_board_mac";
                    mac = <1>;
                };
        };
    };

    avm_gpio {
        compatible = "avm,avm_gpio_generic";
        gpio_avm_led_wlan {
            value = <207>;
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
        };
        gpio_avm_led_plc {
            value = <208>;
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
        };
        gpio_avm_led_pairing {
            value = <209>;
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
        };
        gpio_avm_reset_plc {
            value = <206>;
            param = <AVM_DEF_HW_PARAM_GPIO_OUT_ACTIVE_HIGH>;
        };
        gpio_avm_button_pairing {
            value = <63>;
            param = <AVM_DEF_HW_PARAM_GPIO_IN_ACTIVE_LOW>;
        };
    };
};

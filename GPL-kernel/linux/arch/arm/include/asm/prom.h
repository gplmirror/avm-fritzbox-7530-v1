/*
 *  arch/arm/include/asm/prom.h
 *
 *  Copyright (C) 2009 Canonical Ltd. <jeremy.kerr@canonical.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */
#ifndef __ASMARM_PROM_H
#define __ASMARM_PROM_H

#ifdef CONFIG_OF

extern const struct machine_desc *setup_machine_fdt(unsigned int dt_phys);
extern void __init arm_dt_init_cpu_maps(void);

#else /* CONFIG_OF */

static inline const struct machine_desc *setup_machine_fdt(unsigned int dt_phys)
{
	return NULL;
}

static inline void arm_dt_init_cpu_maps(void) { }

#endif /* CONFIG_OF */

/* AVM non-dt work-arounds follow */

#if defined(CONFIG_ARCH_QCOM)
#include <linux/kernel.h>
#include <linux/printk.h>
#define prom_printf early_printk
#else
extern void prom_printf(const char *fmt, ...);
#endif
extern char *prom_getcmdline(void);
extern char *prom_getenv(char *name);



extern char *arm_get_machine_name(void);
extern void arm_set_machine_name(const char *name);
/* Memory descriptor management. */
#define PROM_MAX_PMEMBLOCKS    32
struct prom_pmemblock {
	unsigned long base; /* Within KSEG0. */
	unsigned int size;  /* In bytes. */
	unsigned int type;  /* free or prom memory */
};


#include <asm/prealloc_memory.h>
#endif /* ASMARM_PROM_H */

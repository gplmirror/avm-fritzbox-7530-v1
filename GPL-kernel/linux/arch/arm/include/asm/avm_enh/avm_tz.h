  /*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#ifndef _AVM_TZ_H_
#define _AVM_TZ_H_

uint32_t avm_get_tz_version(uint32_t * version, uint32_t * modified);
uint32_t avm_is_avm_tz(void);
void setup_avm_fiq_tz(void *dummy __maybe_unused);
void avm_secure_wdt_pet(void);
void avm_secure_wdt_config(u32 enable, u32 bark, u32 bite);

#endif // #ifndef _AVM_TZ_H_


#ifndef _arch_arm_include_asm_performance_h_
#define _arch_arm_include_asm_performance_h_


#define PMCR            "p15, 0, %0, c9, c12, 0"
#define PMCNTENSET      "p15, 0, %0, c9, c12, 1"
#define PMCNTENCLR      "p15, 0, %0, c9, c12, 2"
#define PMSOVSR         "p15, 0, %0, c9, c12, 3"
#define PMSWINC         "p15, 0, %0, c9, c12, 4"
#define PMSELR          "p15, 0, %0, c9, c12, 5"
#define PMCEID0         "p15, 0, %0, c9, c12, 6"
#define PMCEID1         "p15, 0, %0, c9, c12, 7"
#define PMCCNTR         "p15, 0, %0, c9, c13, 0"
#define PMXEVTYPER      "p15, 0, %0, c9, c13, 1"
#define PMCCFILTR       "p15, 0, %0, c9, c13, 1"
#define PMXEVCNTR       "p15, 0, %0, c9, c13, 2"
#define PMUSERENR       "p15, 0, %0, c9, c14, 0"
#define PMINTENSET      "p15, 0, %0, c9, c14, 1"
#define PMINTENCLR      "p15, 0, %0, c9, c14, 2"

#define PM_EVENT_SW_INCR                0x00 
#define PM_EVENT_L1I_CACHE_REFILL       0x01 
#define PM_EVENT_L1I_TLB_REFILL         0x02 
#define PM_EVENT_L1D_CACHE_REFILL       0x03 
#define PM_EVENT_L1D_CACHE              0x04 
#define PM_EVENT_L1D_TLB_REFILL         0x05 
#define PM_EVENT_LD_RETIRED             0x06 
#define PM_EVENT_ST_RETIRED             0x07 
#define PM_EVENT_INST_RETIRED           0x08 
#define PM_EVENT_EXC_TAKEN              0x09 
#define PM_EVENT_EXC_RETURN             0x0A 
#define PM_EVENT_CID_WRITE_RETIRED      0x0B 
#define PM_EVENT_PC_WRITE_RETIRED       0x0C 
#define PM_EVENT_BR_IMMED_RETIRED       0x0D 
#define PM_EVENT_BR_RETURN_RETIRED      0x0E 
#define PM_EVENT_UNALIGNED_LDST_RETIRED 0x0F 
#define PM_EVENT_BR_MIS_PRED            0x10 
#define PM_EVENT_CPU_CYCLES             0x11 
#define PM_EVENT_BR_PRED                0x12 
#define PM_EVENT_MEM_ACCESS             0x13 
#define PM_EVENT_L1I_CACHE              0x14 
#define PM_EVENT_L1D_CACHE_WB           0x15 
#define PM_EVENT_L2D_CACHE              0x16 
#define PM_EVENT_L2D_CACHE_REFILL       0x17 
#define PM_EVENT_L2D_CACHE_WB           0x18 
#define PM_EVENT_BUS_ACCESS             0x19 
#define PM_EVENT_BUS_CYCLES             0x1D 
/*** Following are optional and implementation defined ***/
#define PM_EVENT_L1D_CACHE_LD                    0x40 
#define PM_EVENT_L1D_CACHE_ST                    0x41
#define PM_EVENT_L1D_CACHE_REFILL_LD             0x42
#define PM_EVENT_L1D_CACHE_REFILL_ST             0x43
#define PM_EVENT_L1D_CACHE_REFILL_INNER          0x44
#define PM_EVENT_L1D_CACHE_REFILL_OUTER          0x45
#define PM_EVENT_L1D_CACHE_WB_VICTIM             0x46
#define PM_EVENT_L1D_CACHE_WB_CLEAN              0x47
#define PM_EVENT_L1D_CACHE_INVAL                 0x48
#define PM_EVENT_L1D_TLB_REFILL_LD               0x4C
#define PM_EVENT_L1D_TLB_REFILL_ST               0x4D
#define PM_EVENT_L2D_CACHE_LD                    0x50
#define PM_EVENT_L2D_CACHE_ST                    0x51
#define PM_EVENT_L2D_CACHE_REFILL_LD             0x52
#define PM_EVENT_L2D_CACHE_REFILL_ST             0x53
#define PM_EVENT_L2D_CACHE_WB_VICTIM             0x56
#define PM_EVENT_L2D_CACHE_WB_CLEAN              0x57
#define PM_EVENT_L2D_CACHE_INVAL                 0x58
#define PM_EVENT_BUS_ACCESS_LD                   0x60
#define PM_EVENT_BUS_ACCESS_ST                   0x61
#define PM_EVENT_BUS_ACCESS_SHARED               0x62
#define PM_EVENT_BUS_ACCESS_NOT_SHARED           0x63
#define PM_EVENT_BUS_ACCESS_NORMAL               0x64
#define PM_EVENT_BUS_ACCESS_PERIPH               0x65
#define PM_EVENT_MEM_ACCESS_LD                   0x66
#define PM_EVENT_MEM_ACCESS_ST                   0x67
#define PM_EVENT_UNALIGNED_LD_SPEC               0x68
#define PM_EVENT_UNALIGNED_ST_SPEC               0x69
#define PM_EVENT_UNALIGNED_LDST_SPEC             0x6A
#define PM_EVENT_LDREX_SPEC                      0x6C
#define PM_EVENT_STREX_PASS_SPEC                 0x6D
#define PM_EVENT_STREX_FAIL_SPEC                 0x6E
#define PM_EVENT_STREX_SPEC                      0x6F
#define PM_EVENT_LD_SPEC                         0x70
#define PM_EVENT_ST_SPEC                         0x71
#define PM_EVENT_LDST_SPEC                       0x72
#define PM_EVENT_DP_SPEC                         0x73
#define PM_EVENT_ASE_SPEC                        0x74
#define PM_EVENT_VFP_SPEC                        0x75
#define PM_EVENT_PC_WRITE_SPEC                   0x76
#define PM_EVENT_BR_IMMED_SPEC                   0x78
#define PM_EVENT_BR_RETURN_SPEC                  0x79
#define PM_EVENT_BR_INDIRECT_SPEC                0x7A
#define PM_EVENT_ISB_SPEC                        0x7C
#define PM_EVENT_DSB_SPEC                        0x7D
#define PM_EVENT_DMB_SPEC                        0x7E
#define PM_EVENT_EXC_UNDEF                       0x81
#define PM_EVENT_EXC_SVC                         0x82
#define PM_EVENT_EXC_PABORT                      0x83
#define PM_EVENT_EXC_DABORT                      0x84
#define PM_EVENT_EXC_IRQ                         0x86
#define PM_EVENT_EXC_FIQ                         0x87
#define PM_EVENT_EXC_SMC                         0x88
#define PM_EVENT_EXC_HVC                         0x8A
#define PM_EVENT_EXC_TRAP_PABORT                 0x8B
#define PM_EVENT_EXC_TRAP_DABORT                 0x8C
#define PM_EVENT_EXC_TRAP_OTHER                  0x8D
#define PM_EVENT_EXC_TRAP_IRQ                    0x8E
#define PM_EVENT_EXC_TRAP_FIQ                    0x8F
#define PM_EVENT_EXT_MEM_REQ                     0xC0	
#define PM_EVENT_NON_CACHE_EXT_MEM_REQ           0xC1	
#define PM_EVENT_LINEFILL_PREFETCH               0xC2	
#define PM_EVENT_PREFETCH_LINEFILL_DROP          0xC3	
#define PM_EVENT_ENTER_READ_ALLOC                0xC4	
#define PM_EVENT_READ_ALLOC_MODE                 0xC5	
#define PM_EVENT_ETM_EXT_OUT0                    0xC7	
#define PM_EVENT_ETM_EXT_OUT1                    0xC8	
#define PM_EVENT_DATA_WRITE_STALL                0xC9	
#define PM_EVENT_DATA_SNOOPED                    0xCA	
#define PM_EVENT_LAST                            PM_EVENT_DATA_SNOOPED + 1

union __performance_monitor_control {
    struct _performance_monitor_control {
        unsigned int EnableBit : 1;
        unsigned int CounterReset : 1; /* 2 */
        unsigned int CycleCounterReset : 1; /* 2 */
        unsigned int CycleCounterDivider : 1; /* 3 */
        unsigned int ExportEnable : 1; /* 4 */
        unsigned int DisableCycleCounter : 1; /* 5 */
        unsigned int reserved : 5 ; /* 10:6 */
        unsigned int NumCounters : 5;   /* 15:11 */
        unsigned int IDCode : 8;   /* 23:16 */
        unsigned int ImpCode : 8;   /* 31:24 */
    } Bits;
    volatile unsigned int Register;
};


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline void write_p15_performance_monitor_control(unsigned int value) {
    __asm__ __volatile__ ("mcr " PMCR : : "r" (value));
    return;
}

static inline void write_p15_cycle_counter(unsigned int value) {
    __asm__ __volatile__ ("mcr " PMCCNTR : : "r" (value));
    return;
}

static inline void write_p15_performance_counter(unsigned int counter, unsigned int value) {
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mcr " PMXEVCNTR : : "r" (value));
    return;
}
static inline void write_p15_performance_event_type_with_cnt_reset(unsigned int counter, unsigned int value) {
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mcr " PMXEVTYPER : : "r" (value));
    value = 0;
    __asm__ __volatile__ ("mcr " PMXEVCNTR : : "r" (value));
    return;
}
static inline void p15_reset_performance_counter(unsigned int counter) {
    int value = 0;
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mcr " PMXEVCNTR : : "r" (value));
}
static inline void write_p15_performance_event_type(unsigned int counter, unsigned int value) {
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mcr " PMXEVTYPER : : "r" (value));
    return;
}
static inline unsigned int write_p15_performance_count_enable(unsigned int value) {
    __asm__ __volatile__ ("mcr " PMCNTENSET : : "r" (value));
    return value;
}


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline unsigned int read_p15_performance_monitor_control(void) {
    unsigned int value;
    __asm__ __volatile__ ("mrc " PMCR : "=r" (value));
    return value;
}

static inline unsigned int read_p15_cycle_counter(void) {
    unsigned int value;
    __asm__ __volatile__ ("mrc " PMCCNTR : "=r" (value));
    return value;
}

static inline unsigned int read_p15_performance_counter_with_reset(unsigned int counter) {
    unsigned int value;
    unsigned int resetval = 0;
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mrc " PMXEVCNTR : "=r" (value));
    __asm__ __volatile__ ("mcr " PMXEVCNTR : : "r" (resetval));
    return value;
}
static inline unsigned int read_p15_performance_counter(unsigned int counter) {
    unsigned int value;
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mrc " PMXEVCNTR : "=r" (value));
    return value;
}
static inline unsigned int read_p15_performance_event_type(unsigned int counter) {
    unsigned int value;
    __asm__ __volatile__ ("mcr " PMSELR : : "r" (counter));
    __asm__ __volatile__ ("mrc " PMXEVTYPER : "=r" (value));
    return value;
}
static inline unsigned int read_p15_performance_count_enable(void) {
    unsigned int value;
    __asm__ __volatile__ ("mrc " PMCNTENSET : "=r" (value));
    return value;
}

#endif /*--- #ifndef _arch_arm_include_asm_performance_h_ ---*/

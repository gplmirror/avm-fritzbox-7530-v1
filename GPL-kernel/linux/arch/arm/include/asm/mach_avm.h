#ifndef _mach_avm_h_
#define _mach_avm_h_

#include <linux/avm_reboot_status.h>

/*------------------------------------------------------------------------------------------*\
 * 240-254 char	LOCAL/EXPERIMENTAL USE
 * 240-254 block	LOCAL/EXPERIMENTAL USE
 *         Allocated for local/experimental use.  For devices not
 *         assigned official numbers, these ranges should be
 *         used in order to avoid conflicting with future assignments.
\*------------------------------------------------------------------------------------------*/
#define AVM_DECT_IO_MAJOR	    227
#define AVM_USERMAN_MAJOR	    228
#define KDSLD_USERMAN_MAJOR	    229
#define AVM_TIATM_MAJOR         230
#define TFFS_MAJOR              262
#define AVM_EVENT_MAJOR         261
#define AVM_DEBUG_MAJOR         263
#define WATCHDOG_MAJOR          260
#define KDSLD_MAJOR             243
#define KDSLDPTRACE_MAJOR       244
#define UBIK_MAJOR              245
#define DEBUG_TRACE_MAJOR       246
#define AVM_LED_MAJOR           247
#define AVM_I2C_MAJOR           248
#define YAFFS                   249
#define AVM_AUDIO_MAJOR         250
#define AVM_NEW_LED_MAJOR       251
#define AVM_POWER_MAJOR         252
#define AVM_VINAX_MAJOR		    253
#define AVM_HSK_MAJOR		    254
#define AVM_NET_TRACE_MAJOR     255
#define AVM_ATH_EEPROM              239

/*------------------------------------------------------------------------------------------*\
 * zusaetzlich folgende:
 *
 * 207 char	Compaq ProLiant health feature indicate
 * 220 char	Myricom Myrinet "GM" board
 * 224 char	A2232 serial card
 * 225 char	A2232 serial card (alternate devices)
 * 227 char	IBM 3270 terminal Unix tty access
 * 228 char	IBM 3270 terminal block-mode access
 * 229 char	IBM iSeries virtual console
 * 230 char	IBM iSeries virtual tape
 *
\*------------------------------------------------------------------------------------------*/

#if defined(CONFIG_ARCH_PUMA5) || defined(CONFIG_MACH_PUMA6)
/*--------------------------------------------------------------------------------*\
 * PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  PUMA  
\*--------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _avm_clock_id {
    avm_clock_id_non        = 0x00,
    avm_clock_id_cpu        = 0x01,
    avm_clock_id_system     = 0x02, /*--- DDR and fast-peripherals ---*/
    avm_clock_id_usb        = 0x04,
    avm_clock_id_docsis     = 0x08,
    avm_clock_id_gmii       = 0x10,
    avm_clock_id_vlynq      = 0x20, /*--- vlynq and slow peripherals ---*/
    avm_clock_id_vbus       = 0x40, /*--- vlynq and slow peripherals ---*/
    avm_clock_id_sflash     = 0x80,
    avm_clock_id_tdm        = 0x100
};

extern unsigned int puma_get_clock(enum _avm_clock_id clock_id);
extern unsigned int puma_set_clock(enum _avm_clock_id clock_id, unsigned int clk);

#define avm_get_clock               puma_get_clock
#define avm_set_clock               puma_set_clock
#if defined(CONFIG_ARCH_PUMA5)
#include <arch-avalanche/puma5/puma5.h>
#elif defined(CONFIG_MACH_PUMA6)
#include <arch-avalanche/puma6/puma6.h>
#endif/*--- #elif defined(CONFIG_MACH_PUMA6) ---*/

#include <mach/hw_gpio.h>

#define avm_gpio_init               puma_gpio_init
#define avm_gpio_ctrl               puma_gpio_ctrl
#define avm_gpio_out_bit            puma_gpio_out_bit
#define avm_gpio_out_bit_no_sched   puma_gpio_out_bit_no_sched
#define avm_gpio_in_bit             puma_gpio_in_bit

#endif/*--- #ifdef CONFIG_ARCH_PUMA5 ---*/


/*------------------------------------------------------------------------------------------*\
 * IPQ40XX IPQ40XX IPQ40XX IPQ40XX IPQ40XX IPQ40XX IPQ40XX IPQ40XX IPQ40XX IPQ40XX IPQ40XX
\*------------------------------------------------------------------------------------------*/
#ifdef CONFIG_ARCH_IPQ40XX 
#include <linux/types.h>
#include <clocksource/arm_arch_timer.h>

#define avm_cycles_cpuclock_depend()    0
extern unsigned int ipq40xx_get_cyclefreq(void);
#define avm_get_cyclefreq 	ipq40xx_get_cyclefreq
#define avm_get_clock        avm_get_cyclefreq       
extern unsigned int ipq40xx_get_cycles(void);
#define avm_get_cycles		ipq40xx_get_cycles


#include <mach/avm_gpio.h>

#define avm_gpio_init                dakota_gpio_init
#define avm_gpio_ctrl                dakota_gpio_ctrl
#define avm_gpio_out_bit             dakota_gpio_out_bit
#define avm_gpio_out_bit_no_sched    dakota_gpio_out_bit_no_sched
#define avm_gpio_in_bit              dakota_gpio_in_bit
#define avm_gpio_get_membase         dakota_gpio_get_membase
#define avm_gpio_in_bit_fast         dakota_gpio_in_bit_fast
#define avm_gpio_pinconfig           dakota_gpio_pinconfig
static inline void avm_gpio_set_bitmask(uint64_t mask __maybe_unused, uint64_t value __maybe_unused) {};

// TODO: einfügen?
// char *avm_urlader_env_get_variable(int idx);
// char *avm_urlader_env_get_value_by_id(unsigned int id);
// char *avm_urlader_env_get_value(char *var);
// int avm_urlader_env_set_variable(char *var, char *val);
// int avm_urlader_env_unset_variable(char *var);
// int avm_urlader_env_defrag(void);

/**--------------------------------------------------------------------------------**\
 * jiffies-correction in FASTIRQ-(NMI-)Handler
\**--------------------------------------------------------------------------------**/
extern void avm_tick_jiffies_update(void);

/*--------------------------------------------------------------------------------*\
 * folgende Funktionen muessen vom Kernel bereitgestellt werden (fuer Piglet-Treiber)
\*--------------------------------------------------------------------------------*/
extern void msm_dectuart_init(unsigned int baud, int mode);
extern void msm_dectuart_exit(void);
extern int msm_dectuart_get_char(void);
extern void msm_dectuart_put_char(unsigned char c);
extern void msm_console_stop(void);
extern void msm_console_start(void);

/**--------------------------------------------------------------------------------**\
 * folgende Funktionen muessen vom snd_soc_ipq40xx_avm bereitgestellt werden (fuer pcmlink-Treiber)
\**--------------------------------------------------------------------------------**/
int tdm_if_config_init(unsigned int slots, unsigned int rxdelay, unsigned int txdelay);
void tdm_if_config_exit(void);
int tdm_if_dma_init(void *refhandle, 
                    unsigned int (*TxData)(void *refhandle, void *buf),
                    unsigned int (*RxData)(void *refhandle, void *buf),
                    unsigned int cpu,
                    unsigned int only_rxirq,
                    unsigned int slots,
                    unsigned int fs_per_dma,
                    unsigned int desc_num);
void tdm_if_dma_start(void);
void tdm_if_dma_stop(void);
void tdm_if_dma_exit(void);
void tdm_if_dma_irqcnt(unsigned long *rx_irqcnt, unsigned long *tx_irqcnt);
void tdm_if_dma_irqcnt_reset(void);
void tdm_if_print_status(const char *prefix, char *txt, unsigned int txt_size);
int tdm_if_dma_get_dma_buffer(unsigned char **rx1,
                              unsigned char **rx2, 
                              unsigned char **tx1, 
                              unsigned char **tx2, 
                              unsigned int  *slotoffset);


#endif/*--- #ifdef CONFIG_ARCH_IPQ40XX ---*/
#ifdef CONFIG_ARM
void avm_dma_pool_init(unsigned char *base, unsigned int size);
void *avm_dma_pool_alloc(unsigned int size, dma_addr_t *physaddr);
void avm_dma_pool_free(void *vaddr);

/*--------------------------------------------------------------------------------*\
 * kernelmem_module.c
\*--------------------------------------------------------------------------------*/
enum _module_alloc_type_ {
    module_alloc_type_init,
    module_alloc_type_core,
    module_alloc_type_page,
    module_alloc_type_unknown
};
struct resource;
#ifdef CONFIG_AVM_BOOTMEM
void __init module_alloc_bootmem_init(struct resource *res, unsigned long start_addr);
unsigned long module_alloc_size_list_alloc(unsigned long size, char *name, enum _module_alloc_type_ type);
int module_alloc_size_list_free(unsigned long addr);
char *module_alloc_find_module_name(char *buff, char *end, unsigned long addr);
#endif /* CONFIG_AVM_BOOTMEM */
void *module_alloc(unsigned long size, char *name, enum _module_alloc_type_ type __attribute__ ((unused)));

char *avm_urlader_env_get_variable(int idx);
char *avm_urlader_env_get_value_by_id(unsigned int id);
char *avm_urlader_env_get_value(char *var);
int avm_urlader_env_set_variable(char *var, char *val);
int avm_urlader_env_unset_variable(char *var);
int avm_urlader_env_defrag(void);

#endif /*--- #ifdef CONFIG_ARM ---*/


#endif /*--- #ifndef _mach_avm_h_ ---*/
